<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarkData extends Model
{
    //
    protected $table = "mark_data";
    protected $fillable = [
        'Bitem', 'Mitem', 'Sitem','title','HighScore','lowScore','login','project','percent'
    ];
}
