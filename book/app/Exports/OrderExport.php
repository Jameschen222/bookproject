<?php

namespace App\Exports;

use function Complex\add;
use http\Env\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;

class OrderExport implements FromCollection, WithEvents, WithMultipleSheets, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $department,$cellData, $mergedata, $value, $max, $stumergedata, $ValueOfStuScore, $allscoreposition, $allscoredetail=0, $detail, $valuestartandend, $percent;
    private $teachername;
    private $teacheraccount;
    public function __construct($i, $a,$de)
    {
            $this->teachername = $i;
            $this->teacheraccount = $a;
            $this->department = $de;
    }

    public function title(): string
    {
        return $this->teachername . '老師評分表';
    }


    public function sheets(): array
    {
        $sheets = [];
        if ($this->teachername == 1 and $this->teacheraccount == 1){
            $this->department = session()->get('department');
            $mod = 2;
        }
        else if ($this->teacheraccount == 0 and $this->teachername == 0) {
            $this->department = session()->get('department');
            $mod = 0;
        }else{
            $this->department = $this->teachername;
            $times = $this->teacheraccount;
            $mod = 1;
        }
        if ($mod == 0) {
            $teacheraccount1 = DB::table('grades')->join('flights','grades.Taccount','=','flights.Faccount')->where('grades.department', '=', $this->department)->where('grades.login','=','2')->select('grades.Taccount','flights.Fname')->distinct()->get();
        }
        if ($mod == 1){
            $teacheraccount1 = DB::table('grades')->join('flights','grades.Taccount','=','flights.Faccount')->where('grades.department', '=', $this->department)->where('grades.login','=',$times)->select('grades.Taccount','flights.Fname')->distinct()->get();
        }
        if ($mod == 2){
            $teacheraccount1 = DB::table('grades')->join('flights','grades.Taccount','=','flights.Faccount')->where('grades.department', '=', $this->department)->where('grades.login','=','2')->where('grades.Taccount','=',session()->get('account'))->select('grades.Taccount','flights.Fname')->distinct()->get();
        }
        for ($num = 0; $num < count($teacheraccount1); $num++) {
            $sheets[] = new OrderExport($teacheraccount1[$num]->Fname, $teacheraccount1[$num]->Taccount,$this->department);
        }
        return $sheets;
    }

    public function collection()
    {
        $this->cellData[0][0] = "";
        $this->cellData[0][1] = "";
        $this->cellData[1][0] = "";
        $this->cellData[1][1] = "";
        $sheetnumber = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        ];
        $check = 0;
        $countforstu = 0;
        $daResults = DB::table('mark_data')->select('*')->where('department','=',$this->department)->get();
       // dd($daResults,$this->department,$this->teachername,$this->teacheraccount);
        for ($x = 0, $y = 2, $z=2; $x < count($daResults); $x++, $y++,$z++) {
            if ($daResults[$x]->Sitem == "0") {

                if ($y > 25) {
                    $y = 0;
                    $check = 1;
                }
                if ($check == 0) {
                    $stumergedata[$countforstu++][0] = $sheetnumber[$y]; //合併學生中面相分數用
                }

                if ($check == 1) {
                    $stumergedata[$countforstu++][0] = "A" . $sheetnumber[$y]; //合併學生中面相分數用
                }
                $y--;
                continue;
            } else {
                if ($y > 25) {
                    $y = 0;
                    $check = 1;
                }
                $this->cellData[0][$z] = str_replace("<br>","\n",$daResults[$x]->title);
                $this->cellData[1][$z] = $daResults[$x]->HighScore . "~" . $daResults[$x]->lowScore;

                if ($check == 0) {
                    if ($x != count($daResults) - 1 and $daResults[$x + 1]->Sitem == "0") {
                        $stumergedata[$countforstu - 1][1] = $sheetnumber[$y];

                    } else {
                        $stumergedata[$countforstu - 1][1] = $sheetnumber[$y];
                        $this->allscoreposition = $y+1; //存最後的位置
                    }
                }
                if ($check == 1) {
                    if ($x != count($daResults) - 1 and $daResults[$x + 1]->Sitem == "0") {
                        $stumergedata[$countforstu - 1][1] = "A" . $sheetnumber[$y];
                    } else {
                        $stumergedata[$countforstu - 1][1] = "A" . $sheetnumber[$y];
                        $this->allscoreposition = 25+$y+1; //存最後的位置
                    }
                }
            }
        }
        $this->cellData[1][0] = "";
        $this->cellData[1][1] = "";
        $this->cellData[2][0] = "應試代碼";
        $this->cellData[2][1] = "姓名";
        $tempcount=0;
        $check = 1;
        $count = 0;
        $overcount = 0;
        $counter=0;
        $bug = 0;
        //dd($daResults);
        for ($i = 0, $z = 2; $i < count($daResults); $i++, $z++) {
            if ($i == 0) $temp = $daResults[$i]->Bitem;
            if ($temp == $daResults[$i]->Bitem) {

                if ($daResults[$i]->Sitem == "0") {
                    $z--;
                } else {
                    if ($z <= 25 and $check == 1) {
                        if ($overcount == 0) {
                            $this->mergedata[$i] = "${sheetnumber[$z]}3:";
                            $this->valuestartandend[$counter++] = $sheetnumber[$z];
                            $this->value[$count++] = [$sheetnumber[$z], str_replace("<br>","\n",
                                $daResults[$i-1]->title)." 百分比:".$daResults[$i-1]->percent."%"];
                            $this->percent[$count-1] = $daResults[$i-1]->percent."%";
                            //dd($count,$sheetnumber[$z],$i,$daResults[$i]->title);
                            $temp2 = $i;
                            $check = 0;
                            $this->detail[$tempcount++] = $daResults[$i - 1]->title;
                            $this->allscoredetail++; //計算幾個面向
                        }
                        if ($overcount == 1) {
                            $this->mergedata[$i] = "A${sheetnumber[$z]}3:";
                            $this->valuestartandend[$counter++] = "A${sheetnumber[$z]}";
                            $this->value[$count++] = ["A" . $sheetnumber[$z], str_replace("<br>","\n",$daResults[$i-1]->title)." 百分比:".$daResults[$i-1]->percent."%"];
                            $temp2 = $i;
                            $check = 0;
                        }
                    }
                    if ($z > 25) {
                        $z = 0;
                        $overcount = 1;
                        if ($bug == 1) {
                            $this->mergedata[$i] = "A${sheetnumber[$z]}3:";
                            $this->valuestartandend[$counter++] = "A${sheetnumber[$z]}";
                            $this->value[$count++] = ["A" . $sheetnumber[$z], str_replace("<br>", "\n", $daResults[$i - 1]->title) . " 百分比:" . $daResults[$i - 1]->percent . "%"];
                            $temp2 = $i;
                            $check = 0;
                        }
                    }
                }
            } else {

                $z--;
                if ($check == 0) {
                    if ($overcount == 0 and isset($temp2)) {
                        $this->mergedata[$temp2] = $this->mergedata[$temp2] . $sheetnumber[$z] . "3";
                        $this->valuestartandend[$counter - 1] = $this->valuestartandend[$counter - 1] . "." . $sheetnumber[$z];
                        if ($sheetnumber[$z] == "Z")
                            $bug = 1;
                        $check = 1;
                    }
                    if ($overcount == 1 and isset($temp2)){
                        $this->mergedata[$temp2] = $this->mergedata[$temp2] . "A".$sheetnumber[$z] . "3";
                        $this->valuestartandend[$counter - 1] = $this->valuestartandend[$counter - 1] . "." . "A".$sheetnumber[$z];
                        $check = 1;
                    }
                }
                $temp = $daResults[$i]->Bitem;
            }
            if ($i == count($daResults) - 1) {
                if ($z > 25) {
                    $z = 0;
                    $overcount = 1;
                }
                if ($overcount == 0) {
                    $this->mergedata[$temp2] = $this->mergedata[$temp2] . $sheetnumber[$z] . "3";
                    $this->valuestartandend[$counter-1] = $this->valuestartandend[$counter-1] .".". $sheetnumber[$z];
                    $this->max = $sheetnumber[$z] . "3";
                }
                if ($overcount == 1) {
                    $this->mergedata[$temp2] = $this->mergedata[$temp2] . "A" . $sheetnumber[$z] . "3";
                    $this->valuestartandend[$counter-1] = $this->valuestartandend[$counter-1] .".A". $sheetnumber[$z];
                    $this->max = "A" . $sheetnumber[$z] . "3";
                }
            }
        }//dd(explode(".",$this->valuestartandend[0]));
        //dd($this->mergedata,$this->valuestartandend);
        $stusnum = DB::table('grades')->join('students','grades.Snum','=','students.Snum')->select('grades.Snum','students.category')->distinct()->where('grades.Taccount', '=', $this->teacheraccount)->orderByDesc('students.category')->orderBy('grades.Snum','asc')->get();
        for ($sheetlocation = 4, $n = 0; $n < count($stusnum); $sheetlocation++, $n++) {
            $this->cellData[$sheetlocation][0] = $stusnum[$n]->Snum;
            $name = DB::table('students')->select('Sname')->where('Snum', '=', $stusnum[$n]->Snum)->orderByDesc('category')->orderBy('Snum','asc')->get();
            if (isset( $name[0])) {
                $this->cellData[$sheetlocation][1] = $name[0]->Sname;
            }else{
                $this->cellData[$sheetlocation][1] = "資料錯誤，請確認學生資料是否正確"; //防呆
            }
            $nowscore = DB::table('grades')->join('students','grades.Snum','=','students.Snum')->select('grades.score','students.Snum')->where('grades.Taccount', '=', $this->teacheraccount)->where('grades.Snum', '=', $stusnum[$n]->Snum)->orderByDesc('students.category')->orderBy('grades.Snum','asc')->get();
            $range = count($nowscore);
            for ($x = 0, $j = 0, $t = $sheetlocation; $j < $range; $x++, $j++) {
                $this->ValueOfStuScore[$n][$x][0] = $nowscore[$x]->score;
                $this->ValueOfStuScore[$n][$x][1] = $stumergedata[$j][0] . $t;
            }
        }

        for ($n = 0, $sheetlocation = 4; $n < count($stusnum); $sheetlocation++, $n++) {
            for ($x = 0; $x < count($stumergedata); $x++) {
                $this->stumergedata[$n][$x] =
                    $stumergedata[$x][0] . $sheetlocation . ":" .
                    $stumergedata[$x][1] . $sheetlocation;
                $this->max = $stumergedata[$x][1] . $sheetlocation;
            }
        }
        for ($x = 0; $x < count($this->stumergedata); $x++) {
            $this->mergedata = array_merge($this->mergedata, $this->stumergedata[$x]);
        }
        return collect($this->cellData);
    }

    public function registerEvents(): array
    {
        // if (isset($this->mergedata) and isset($this->value)) {

        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheetnumber = [
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
                ];
                if ($this->allscoreposition>25){
                    for ($x = 0,$y = $this->allscoreposition-25 ; $x<$this->allscoredetail ; $x++,$y++){
                        $event->sheet->getDelegate()->setCellValue("A".$sheetnumber[$y]."1", $this->detail[$x]."總分");
                    }
                }
                else{
                    for ($x = 0,$y = $this->allscoreposition ; $x<$this->allscoredetail ; $x++,$y++){
                        $event->sheet->getDelegate()->setCellValue($sheetnumber[$y]."1", $this->detail[$x]."總分");
                        $end = $y;
                    }
                }

                for ($x=0,$y = $end+1;$x<$this->allscoredetail;$x++,$y++){
                    if ($y>25)
                        $event->sheet->getDelegate()->setCellValue("A".$sheetnumber[$y-25]."1", $this->detail[$x]."百分比總分");
                    else
                        $event->sheet->getDelegate()->setCellValue($sheetnumber[$y]."1", $this->detail[$x]."百分比總分");
                }

                $stusnum = DB::table('grades')->join('students','grades.Snum','=','students.Snum')->select('grades.Snum','students.category')->distinct()->where('grades.Taccount', '=', $this->teacheraccount)->orderByDesc('students.category')->orderBy('grades.Snum','asc')->get();
                for ($n = 0,$j=4; $n < count($stusnum); $n++,$j++) {
                    for ($y = 0; $y < count($this->ValueOfStuScore[$n]); $y++) {
                        $event->sheet->getDelegate()->setCellValue($this->ValueOfStuScore[$n][$y][1], $this->ValueOfStuScore[$n][$y][0]);
                        //dd($this->ValueOfStuScore[$n][$y][1], $this->ValueOfStuScore[$n][$y][0]);
                    }
                    if ($this->allscoreposition>25){
                        for ($x = 0,$f = $this->allscoreposition-25 ; $x<$this->allscoredetail ; $x++,$f++) {
                            $event->sheet->getDelegate()->setCellValue("A".$sheetnumber[$f].$j, "=SUM(".explode(".",$this->valuestartandend[$x])[0].$j.":".explode(".",$this->valuestartandend[$x])[1].$j.")");
                            $event->sheet->getDelegate()->setCellValue("A".$sheetnumber[$f+$this->allscoredetail].$j, "=SUM(".explode(".",$this->valuestartandend[$x])[0].$j.":".explode(".",$this->valuestartandend[$x])[1].$j.")*".$this->percent[$x]);
                        }
                    }else{
                        for ($x = 0,$f = $this->allscoreposition ; $x<$this->allscoredetail ; $x++,$f++) {
                            $event->sheet->getDelegate()->setCellValue($sheetnumber[$f].$j, "=SUM(".explode(".",$this->valuestartandend[$x])[0].$j.":".explode(".",$this->valuestartandend[$x])[1].$j.")");
                            $event->sheet->getDelegate()->setCellValue($sheetnumber[$f+$this->allscoredetail].$j, "=SUM(".explode(".",$this->valuestartandend[$x])[0].$j.":".explode(".",$this->valuestartandend[$x])[1].$j.")*".$this->percent[$x]);
                        }
                    }
                }

                for ($x = 0; $x < count($this->value); $x++)
                    $event->sheet->getDelegate()->setCellValue($this->value[$x][0] . "3", $this->value[$x][1]);

                $event->sheet->getDelegate()->setMergeCells($this->mergedata);
                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(100);
                $event->sheet->getDelegate()->getColumnDimension("A")->setWidth(13);
                $event->sheet->getDelegate()->getColumnDimension("B")->setWidth(8);
                $event->sheet->getDelegate()->getStyle('A1:' . $this->max)->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A1:' . $this->max)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
            }

        ];
        /* }else{
            return redirect(dd('error'));
        }*/
    }
}
