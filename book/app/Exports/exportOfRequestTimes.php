<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class exportOfRequestTimes implements FromCollection, WithEvents, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $MitemOnly,$cellData, $mergedata, $value, $max, $stumergedata;
    public function title(): string
    {
        $daResults =  DB::table('mark_data')->select('project')->where('department','=',session()->get('department'))->where('login','=','2')->distinct()->get();
        if ($daResults[0]->project == null or $daResults[0]->project == " ") {
            return "管道名稱錯誤:erroe資料庫沒有名稱";
        }else{
            return $daResults[0]->project;
        }
    }

    public function collection()
    {
        $daResults=  DB::table('mark_data')->select('Bitem', 'Mitem', 'Sitem', 'title', 'HighScore', 'LowScore','percent')->where('department','=',session()->get('department'))->where('login','=','2')->get();
        if ($daResults[0]->Bitem == "0" and $daResults[0]->Mitem == "0" and $daResults[0]->Sitem == "0" and $daResults[0]->title == null and $daResults[0]->HighScore == null and $daResults[0]->LowScore == null and $daResults[0]->percent == null) {
            $this->mergedata[0][0] = "資料庫無資料，請先完整新增尺規資料，若已新增請洽維修人員";
        }else {
            $sheetnumber = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
            ];
            $check = 0;
            $this->cellData[0][0] = "";
            //dd($daResults);
            for ($x = 0, $y = 0, $z = 0, $count = 0; $x < count($daResults); $x++, $y++, $z++) {
                if ($daResults[$x]->Sitem == "0") {

                    if ($y > 25) {
                        $y = 0;
                        $check = 1;
                    }
                    if ($check == 0) {
                        $stumergedata[$count++][0] = $sheetnumber[$y]; //合併學生中面相分數用
                        $this->MitemOnly[$count - 1][1] = "Mitem";
                        $this->MitemOnly[$count - 1][0] = $sheetnumber[$y] . "1";
                    }

                    if ($check == 1) {
                        $stumergedata[$count++][0] = "A" . $sheetnumber[$y]; //合併學生中面相分數用
                        $this->MitemOnly[$count - 1][1] = "Mitem";
                        $this->MitemOnly[$count - 1][0] = "A" . $sheetnumber[$y] . "1";
                    }
                    $y--;
                    continue;
                } else {
                    $this->cellData[1][$z] = str_replace("<br>", "\n", $daResults[$x]->title);
                    $this->cellData[2][$z] = $daResults[$x]->HighScore . "~" . $daResults[$x]->LowScore;
                    if ($y > 25) {
                        $y = 0;
                        $check = 1;
                    }
                    if ($check == 0) {
                        if ($x != count($daResults) - 1 and $daResults[$x + 1]->Sitem == "0") {
                            $stumergedata[$count - 1][1] = $sheetnumber[$y];

                        } else {
                            $stumergedata[$count - 1][1] = $sheetnumber[$y];
                        }
                    }
                    if ($check == 1) {
                        if ($x != count($daResults) - 1 and $daResults[$x + 1]->Sitem == "0") {
                            $stumergedata[$count - 1][1] = "A" . $sheetnumber[$y];
                        } else {
                            $stumergedata[$count - 1][1] = "A" . $sheetnumber[$y];
                        }
                    }
                }
            }
            for ($x = 0; $x < $count; $x++)
                $this->stumergedata[$x] = $stumergedata[$x][0] . "1:" . $stumergedata[$x][1] . "1";
            $check = 1;
            $temp = 0;
            $overcount = 0;
            $bug = 0;
            for ($x = 0, $y = 0, $count = 0; $x < count($daResults); $x++, $y++) {
                if ($x == 0) $tempBitem = $daResults[$x]->Bitem;
                if ($tempBitem == $daResults[$x]->Bitem) {
                    if ($daResults[$x]->Sitem == "0") {
                        $y--;
                    } else {
                        if ($y <= 25 and $check == 1) {
                            if ($overcount == 0) {
                                $this->mergedata[$x] = "${sheetnumber[$y]}4:";
                                $temp = $x;
                                $this->value[$count++] = ["${sheetnumber[$y]}4", $daResults[$x - 1]->title . " 百分比:" . $daResults[$x - 1]->percent . "%"];
                                $check = 0;
                            }
                            if ($overcount == 1) {
                                $this->mergedata[$x] = "A${sheetnumber[$y]}4:";
                                $temp = $x;
                                $this->value[$count++] = ["A" . "${sheetnumber[$y]}4", $daResults[$x - 1]->title . " 百分比:" . $daResults[$x - 1]->percent . "%"];
                                $check = 0;
                            }
                        }
                        if ($y > 25) {
                            $y = 0;
                            $overcount = 1;
                            if ($bug == 1) {
                                $this->mergedata[$x] = "A${sheetnumber[$y]}4:";
                                $temp = $x;
                                $this->value[$count++] = ["A" . "${sheetnumber[$y]}4", $daResults[$x - 1]->title . " 百分比:" . $daResults[$x - 1]->percent . "%"];
                                $check = 0;
                            }
                        }
                    }
                } else {
                    $y--;
                    if ($check == 0) {
                        if ($overcount == 0) {
                            $this->mergedata[$temp] = $this->mergedata[$temp] . $sheetnumber[$y] . "4";
                            if ($sheetnumber[$y] == "Z")
                                $bug = 1;
                            $check = 1;
                        }
                        if ($overcount == 1) {
                            $this->mergedata[$temp] = $this->mergedata[$temp] . "A" . $sheetnumber[$y] . "4";
                            $check = 1;
                        }
                    }
                    $tempBitem = $daResults[$x]->Bitem;
                }
                if ($x == count($daResults) - 1) {
                    if ($y > 25) {
                        $y = 0;
                        $overcount = 1;
                    }
                    if ($overcount == 0) {
                        $this->mergedata[$temp] = $this->mergedata[$temp] . $sheetnumber[$y] . "4";
                        $this->max = $sheetnumber[$y] . "4";
                    }
                    if ($overcount == 1) {
                        $this->mergedata[$temp] = $this->mergedata[$temp] . "A" . $sheetnumber[$y] . "4";
                        $this->max = "A" . $sheetnumber[$y] . "4";
                    }
                }

            }
            $this->mergedata = array_merge($this->mergedata, $this->stumergedata);
            //dd($this->mergedata);
            return collect($this->cellData);
        }
    }

    public function registerEvents(): array
    {
        // if (isset($this->mergedata) and isset($this->value)) {

        return [
            AfterSheet::class => function (AfterSheet $event) {
                for ($x = 0; $x < count($this->value); $x++)
                    $event->sheet->getDelegate()->setCellValue($this->value[$x][0] , $this->value[$x][1]);
                for ($x = 0; $x < count($this->MitemOnly); $x++)
                    $event->sheet->getDelegate()->setCellValue($this->MitemOnly[$x][0] , $this->MitemOnly[$x][1]);
                $event->sheet->getDelegate()->setMergeCells($this->mergedata);
                $event->sheet->getDelegate()->getStyle('A1:' . $this->max)->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A1:' . $this->max)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
            }

        ];
        /* }else{
            return redirect(dd('error'));
        }*/
    }
}
