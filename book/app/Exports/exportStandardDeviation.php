<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class exportStandardDeviation implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $celldata = [];
        //DB::table('final_standarddeviation')->where('Taccount','名額類別')->where('department','華文系')->delete();
        $export = DB::table('final_standarddeviation')->select('*')->get();
        $celldata[0][0] = "老師帳號";
        $celldata[0][1] = "系所";
        $celldata[0][2] = "標準差";
        for ($x = 0;$x<count($export);$x++){
            $celldata[$x+1][0] = $export[$x]->Taccount;
            $celldata[$x+1][1] = $export[$x]->department;
            $celldata[$x+1][2] = $export[$x]->StandardDeviation;
        }
        return collect($celldata);
    }
}
