<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Flight extends Model
{
    //
    protected $guard = 'flights';
    protected $table = "flights";
    protected $fillable = [
        'Faccount', 'password', 'Fname', 'position', 'department', 'login','password'
    ];
    protected $hidden = [
        'password'
    ];
    public static function auths()
    {
        if (session()->get('account') == "U0633117" || session()->get('account') == "U0633126" || session()->get('account') == "U0633010") {
            return true;
        }
    }
}
