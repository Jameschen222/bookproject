<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;
use App\MarkData;
use App\Student;
use App\StudentDepartment;
use App\Grade;
use App\Match;
use \Cache;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Location;
use Illuminate\Http\RedirectResponse;

class JudgeStandardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (session()->get('status') == "office") {
            return response(view('Department.Score_range'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
        } else {
            setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
            $url = "https://sso.nuu.edu.tw/api/logout.php";
            $data_array = array("account" => session()->get('account'));
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data_array)
                )
            );
            $context  = stream_context_create($options);
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }

        // return view('test');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        // $keys = DB::table("flights")->select('*')->where('login', '=', "office")->pluck("Faccount");
        // if ($request->cookie('key') != "") {
        //     // $request->cookie('key')
        //     foreach ($keys as $key => $value) {
        //         if (Hash::check($value, $request->cookie('key'))) {
        //             // The passwords match...
        //             $accountdata = DB::table("flights")->select('*')->where('Faccount', '=', $value)->get();
                    // $department = $accountdata[0]->department;
                    // $department = '資訊管理學系';
        if(session()->get('status')=="office"){
            $department = session()->get('department');          
            $Score['allItems'] = MarkData::where('department', $department)->where('Mitem', '0')->where('Sitem', '0')->where('login', '2')->count();
            $Score['project'] = MarkData::where('department', $department)->where('Mitem', '0')->where('Sitem', '0')->where('login', '2')->value('project');
            // dd(MarkData::where('department', $department)->where('Mitem', '0')->where('Sitem', '0')->where('login', '2')->doesntExist());
            if(MarkData::where('department', $department)->where('Mitem', '0')->where('Sitem', '0')->where('login', '2')->doesntExist()){
                // dd("123");
                $data['department'] = $department;
                $data['Bitem'] = 0;
                $data['Mitem'] = 0;
                $data['Sitem'] = 0;
                $data['login'] = 2;
                $data['project'] = trim("");
                MarkData::insert($data);
                
            }
            $Bitem = MarkData::where('department', $department)->where('Mitem', '0')->where('Sitem', '0')->where('login', '2')->pluck('id'); 
            // dd($Bitem);
            
            // dd(MarkData::where('department', $department)->where('Mitem', '0')->where('Sitem', '0')->where('login', '2')->get(),$Score['allItems'],$Score['project'],$Bitem,MarkData::where('id', $Bitem[0])->get());
            $o = -1;
            foreach ($Bitem as $a) {
                $BitemNum = MarkData::where('id', $a)->value('Bitem');
                // dump($BitemNum);
                $items['ItemsTitle'] = MarkData::where('id', $a)->value('title');
                $items['ItemsNumber'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Sitem', '0')->where('login', '2')->count();
                $items['percent'] = (int)MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', '0')->where('login', '2')->where('Sitem', '0')->value('percent');
                $Mitem = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Sitem', '0')->where('login', '2')->pluck('id');
                $n = -1;
                $itemdata = null;
                foreach ($Mitem as $b) {
                    $MitemNum = MarkData::where('id', $b)->value('Mitem');
                    $item['number'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', '!=', '0')->where('login', '2')->count();
                    $Sitem = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', '!=', '0')->where('login', '2')->pluck('id');
                    $m = -1;

                    unset($ScoreRangedata);
                    foreach ($Sitem as $c) {
                        $SitemNum = MarkData::where('id', $c)->value('Sitem');
                        $ScoreRange['content'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', $SitemNum)->where('login', '2')->value('title');
                        $ScoreRange['max'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', $SitemNum)->where('login', '2')->value('HighScore');
                        $ScoreRange['min'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', $SitemNum)->where('login', '2')->value('lowScore');
                        
                        $ScoreRangedata[++$m] = $ScoreRange;
                    }
                    if (isset($ScoreRangedata)) $item['ScoreRange'] = $ScoreRangedata;
                    else {
                        $item['ScoreRange'] = [];
                    }
                    $itemdata[++$n] = $item;
                }
                if (isset($itemdata)) $items['item'] = $itemdata;
                $itemsdata[++$o] = $items;
            }
            if (isset($itemsdata)) {
                $Score['items'] = $itemsdata;
            }
            $Score['oldprojects']=MarkData::where('department', $department)->where('login','!=','2')->select('login')->distinct()->get();
            
            return json_encode($Score);
        }
        //         }
        //     }
        // }

        return json_encode([]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $department = session()->get('department');
        MarkData::where('department', $department)->where('login','2')->delete();
        if(isset($request->project) && isset($request->newproject)){
            $data=MarkData::where('department', $department)->where('login',$request->project)->get();
            $insert_data=[];
            $Bitem=array();
            $Mitem=array();
            $Sitem=array();

            foreach ($data as $key => $value) {
                $insert_data= array(
                    'Bitem' => $value['Bitem'],
                    'Mitem' => $value['Mitem'],
                    'Sitem' => $value['Sitem'],
                    'title' => $value['title'],
                    'HighScore' => $value['HighScore'],
                    'lowScore' => $value['lowScore'],
                    'login' => '2',
                    'project' => $request->newproject,
                    'department' => $department,
                    'percent' => $value['percent'],
                );
                MarkData::insert($insert_data);
                // dump($insert_data);
                array_push($Bitem,$value['Bitem']);
                array_push($Mitem,$value['Mitem']);
                array_push($Sitem,$value['Sitem']);
            }
        }
        $count = count($Bitem);
        // dd($Bitem,$Mitem,$Sitem);
        for ($a = 0; $a < $count; $a++) {
            //預設評分
            if ($Sitem[$a] == "0") {
                $Snum = StudentDepartment::where("department", $department)->where('login', '2')->pluck('Snum');
                foreach ($Snum as $b) {
                    $Gradedata['Snum'] = $b;
                    $Gradedata['department'] = $department;
                    $Gradedata['Bitem'] = $Bitem[$a];
                    $Gradedata['Mitem'] = $Mitem[$a];
                    $Gradedata['login'] = 2;
                    $Gradedata['score'] = 0;
                    $Gradedata['block'] = -1;
                    $Taccount = Match::where("department", $department)->where("Snum", $b)->where("choose", "1")->where('login', '2')->pluck('Taccount');
                    foreach ($Taccount as $c) {
                        $Gradedata['Taccount'] = $c;
                        Grade::insert($Gradedata);
                    }
                }
            }
        }

    

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //test
        // dd($request->all());
        $department = session()->get('department');
        $alert="錯誤:";
        // $department = '資訊管理學系';
        Markdata::where('department', $department)->where('login', '2')->delete();
        Grade::where('department', $department)->where('login', '2')->delete();
        // dd($request);
        $pass1=false;
        $pass2=true;
        $back = $_SERVER["HTTP_REFERER"];
        if(MarkData::where('department', $department)->where('project', $request->project)->exists()){
            $alert=$alert.'管道重複，請修改管道名稱'.'  ';
        }else{
            $pass1=true;
        }
        if(isset($request->Bitem)){
            $count = count($request->Bitem);
        }else{
            $count = 1;
        }
        $Bitem = $request->Bitem?$request->Bitem :[0];
        $Mitem = $request->Mitem?$request->Mitem :[0];
        $Sitem = $request->Sitem?$request->Sitem :[0];
        $title = $request->title?$request->title :[0];
        $HighScore = $request->HighScore?$request->HighScore :[0];
        $lowScore = $request->lowScore?$request->lowScore :[0];
        $project=$request->project?trim($request->project) :[0];
        $percent = $request->percent?$request->percent :[0];
        // dd($percent);
        for ($a = 0; $a < $count; $a++) {
            $data['department'] = $department;
            $data['Bitem'] = $Bitem[$a];
            $data['Mitem'] = $Mitem[$a];
            $data['Sitem'] = $Sitem[$a];
            if( $Sitem[$a]==0 && !isset($Sitem[$a+1]) || $Sitem[$a]==0 && $Sitem[$a+1]!=1 ){
                continue;
            }
            $data['title'] = $title[$a];
            if ($Mitem[$a] > 0 & $Sitem[$a] == 0) { //中面相不為0
                $bigtitle = Markdata::where('Bitem', $Bitem[$a])->where('Mitem', '0')->where('Sitem', '0')->where('login', '2')->value('title');
                $data['title'] = $bigtitle . '-' . $Mitem[$a];
            }
            if( $HighScore[$a]!=null && $lowScore[$a]!=null && $HighScore[$a]<=$lowScore[$a] ){
                $alert=$alert.'面向'.($Bitem[$a]+1).'最低分小於或等於最高分'.'  ';
                $pass2=false;
            }else{
                $data['HighScore'] = $HighScore[$a];
                $data['lowScore'] = $lowScore[$a];
            }
            $data['login'] = 2;
            $data['project']=$project;
            $data['percent']=(int)$percent[$a];
            Markdata::insert($data);


            //預設評分
            if ($Sitem[$a] == "0") {
                $Snum = StudentDepartment::where("department", $department)->where('login', '2')->pluck('Snum');
                foreach ($Snum as $b) {
                    $Gradedata['Snum'] = $b;
                    $Gradedata['department'] = $department;
                    $Gradedata['Bitem'] = $Bitem[$a];
                    $Gradedata['Mitem'] = $Mitem[$a];
                    $Gradedata['login'] = 2;
                    $Gradedata['score'] = 0;
                    $Gradedata['block'] = -1;
                    $Taccount = Match::where("department", $department)->where("Snum", $b)->where("choose", "1")->where('login', '2')->pluck('Taccount');
                    foreach ($Taccount as $c) {
                        $Gradedata['Taccount'] = $c;
                        Grade::insert($Gradedata);
                    }
                }
            }
        }
        if( $pass1&&$pass2){
            Cache::put('datastatus', '更新成功' , 3);
        }else{
            Cache::put('datastatus', $alert , 3);
        }
        
        return redirect()->route('standard.index');
        
    }
}
