<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Flight;
use Illuminate\Support\Facades\Hash;
use function MongoDB\BSON\toJSON;

class ForIR extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (session()->get('status')=="Admin") {
            //cookie="bwerqer123123"
            return response(view('admin.forIR'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
        } else {
            setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
            $url = "https://sso.nuu.edu.tw/api/logout.php";
            $data_array = array("account" => session()->get('account'));
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data_array)
                )
            );
            $context  = stream_context_create($options);
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $datas = array_except($data, ['_token']);
        // dd($datas);
        $datas['Fname'] = '管理者';
        $datas['position'] = 'T';
        $datas['department'] = '管理者';
        $datas['login'] = 'IR';
        DB::table('flights')->insert($datas);
        return redirect()->route('ForIR.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $keys = DB::table("flights")->select('*')->where('login', '=', "Admin")->pluck("Faccount");
        if ($request->cookie('key') != "") {
            // $request->cookie('key')
            foreach ($keys as $key => $value) {
                if (Hash::check($value, $request->cookie('key'))) {
                    // The passwords match...
                    $users = DB::table('flights')->where('login', '=', 'IR')->get();
                    return json_encode($users);
                }
            }
        }
        return json_encode([]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if (isset($request->Faccount)) {
            $datas['Faccount'] = $request->Faccount;
        }
        DB::table('flights')
            ->where('id', $request->id)
            ->update($datas);
        return redirect()->route('ForIR.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $flight = DB::table('flights')->where('id', $request->id)->delete();
        return redirect()->route('ForIR.index');
    }
}
