<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class compareController extends Controller
{
    function index(){
        if (session()->get('status')=="IR") {
            return response(view('IR.standard'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
        }
        switch (session()->get('status')) {
            case 'Admin':
                return redirect('/admin');
            case 'head':
                return redirect('/detail');
            case 'teacher':
                return redirect('/judge');
            case 'office':
                return redirect('/head');
            default:
                return  redirect('Undefined');
        }
        setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
        $url = "https://sso.nuu.edu.tw/api/logout.php";
        $data_array = array("account" => session()->get('account'));
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data_array)
            )
        );
        $context  = stream_context_create($options);
        session()->flush();
        session()->put('error', '權限不足 請重新登入');
        return view('welcome');
    }
    function count($request){
        if (session()->get('status') == 'IR' or session()->get('status') == 'head') {
            if ($request->department == null or $request->dif == null) {
                $department = DB::table('teacher_standard_deviation')->select('department')->distinct()->get();

                return json_encode($department);
            } else if ($request->department != null and $request->get('dif') != null) {
                $student = DB::table('teacher_standard_deviation')->select('Snum', 'name')->where('department', $request->department)->distinct()->get();
                $teacher = DB::table('teacher_standard_deviation')->select('Taccount')->where('department', $request->department)->distinct()->get();
                $warnstudent = [];
                foreach ($student as $key => $value) {
                    $objs = [];
                    $max = 0;
                    $min = 9999;
                    for ($y = 0; $y < count($teacher); $y++) {
                        $obj = [];
                        $obj["sum"] = round(DB::table('teacher_standard_deviation')->select('score')->where('Taccount', $teacher[$y]->Taccount)->where('Snum', $value->Snum)->where('department', $request->department)->sum('score'), 2);
                        $max = max($max, $obj["sum"]);
                        $min = min($min, $obj["sum"]);
                        $obj["teacher"] = $teacher[$y]->Taccount;
                        array_push($objs, (object)$obj);
                        unset($obj);
                    }
                    if ($max - $min >= (int)$request->dif) {
                        array_push($warnstudent, (object)[
                            'studentname' => $value->name,
                            'studentSnum' => $value->Snum,
                            'teacher' => $objs
                        ]);
                    }
                    unset($objs);
                }
                // for ($x = 0;$x < count($student);$x++){
                //     array_push($warnstudent,{})
                //     for ($y = 0;$y < count($teacher);$y++){

                //         $allStuTotalScore[$x][$y] = DB::table('teacher_standard_deviation')->select('*')->where('Taccount',$teacher[$y]->Taccount)->where('Snum',$student[$x]->Snum)->sum('score');
                //     }
                // }
                //     for ($y = 0 ;$y<count($student);$y++) {
                //         $max = collect([$allStuTotalScore[0][$y],$allStuTotalScore[1][$y],$allStuTotalScore[2][$y]])->max();
                //         $min = collect([$allStuTotalScore[0][$y],$allStuTotalScore[1][$y],$allStuTotalScore[2][$y]])->min();
                //         if ($max - $min >=10){
                //             $warnstudent[$count++] = $y;
                //         }
                //     }
                //     for ($x = 0;$x<count($teacher);$x++){

                //     }
                //     for ($x = 0;$x<count($warnstudent);$x++){
                //         $studentdata[$x] = (object)array(
                //             "studentid" => $student[$warnstudent[$x]]->Snum,
                //             "studentname" =>
                //             "teacher" => $teacher[],
                //         );
                //     }

                // dd($student,$teacher,$allStuTotalScore,$studentdata);
                //warnstudent為有問題學生
                return json_encode($warnstudent);
            }
        }else{
            if ($request->dif == null) {
                cache()->put('error', '請給差分分數(dif)', 3);
                return back();
            } else{
                $student = DB::table('teacher_standard_deviation')->select('Snum', 'name')->where('department', $request->department)->distinct()->get();
                $teacher = DB::table('teacher_standard_deviation')->select('Taccount')->where('department', $request->department)->distinct()->get();
                $warnstudent = [];
                foreach ($student as $key => $value) {
                    $objs = [];
                    $max = 0;
                    $min = 9999;
                    for ($y = 0; $y < count($teacher); $y++) {
                        $obj = [];
                        $obj["sum"] = round(DB::table('teacher_standard_deviation')->select('score')->where('Taccount', $teacher[$y]->Taccount)->where('Snum', $value->Snum)->where('department', $request->department)->sum('score'), 2);
                        $max = max($max, $obj["sum"]);
                        $min = min($min, $obj["sum"]);
                        $obj["teacher"] = $teacher[$y]->Taccount;
                        array_push($objs, (object)$obj);
                        unset($obj);
                    }
                    if ($max - $min >= (int)$request->dif) {
                        array_push($warnstudent, (object)[
                            'studentname' => $value->name,
                            'studentSnum' => $value->Snum,
                            'teacher' => $objs
                        ]);
                    }
                    unset($objs);
                }
                // for ($x = 0;$x < count($student);$x++){
                //     array_push($warnstudent,{})
                //     for ($y = 0;$y < count($teacher);$y++){

                //         $allStuTotalScore[$x][$y] = DB::table('teacher_standard_deviation')->select('*')->where('Taccount',$teacher[$y]->Taccount)->where('Snum',$student[$x]->Snum)->sum('score');
                //     }
                // }
                //     for ($y = 0 ;$y<count($student);$y++) {
                //         $max = collect([$allStuTotalScore[0][$y],$allStuTotalScore[1][$y],$allStuTotalScore[2][$y]])->max();
                //         $min = collect([$allStuTotalScore[0][$y],$allStuTotalScore[1][$y],$allStuTotalScore[2][$y]])->min();
                //         if ($max - $min >=10){
                //             $warnstudent[$count++] = $y;
                //         }
                //     }
                //     for ($x = 0;$x<count($teacher);$x++){

                //     }
                //     for ($x = 0;$x<count($warnstudent);$x++){
                //         $studentdata[$x] = (object)array(
                //             "studentid" => $student[$warnstudent[$x]]->Snum,
                //             "studentname" =>
                //             "teacher" => $teacher[],
                //         );
                //     }

                // dd($student,$teacher,$allStuTotalScore,$studentdata);
                //warnstudent為有問題學生
                return json_encode($warnstudent);
            }
        }
    }

    function show(Request $request){
        return $this->count($request);
    }

}
