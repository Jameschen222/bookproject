<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentDepartment;
use App\Student;
use \Cache;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (session()->get('status') == "office" or session()->get('status') == "IR"  ) {
            //cookie="bwerqer123123"
            return response(view('Department.student'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
            
        } else {
            //            setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
            //             $url = "https://sso.nuu.edu.tw/api/logout.php";
            //             $data_array = array("account" => session()->get('account'));
            //             $options = array(
            //                 'http' => array(
            //                     'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            //                     'method'  => 'POST',
            //                     'content' => http_build_query($data_array)
            //                 )
            //             );
            //             $context  = stream_context_create($options);
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
        // return view('test');
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //test 用   插入配對為0的資料
        $department = session()->get('department');
        $teacher = DB::table('teachers')->where('department', $department)->where('login','2')->pluck('Taccount');
        $student = DB::table('students')->where('department', $department)->where('login','2')->pluck('Snum');
        foreach ($teacher as $a) {
            foreach ($student as $b) {
                $match = DB::table('matches')->where('Taccount', $a)->where('Snum', $b)->where('login','2')->doesntExist();
                // dd($match);
                if ($match) {
                    $datas['Snum'] = $b;
                    $datas['Taccount'] = $a;
                    $datas['department'] = $department;
                    $datas['date'] = "2019-8-27";
                    $datas['choose'] = '0';
                    $datas['login'] = '2';
                    DB::table('matches')->insert($datas);
                }
            }
        }
        Cache::put('datastatus', '新增成功' , 3);
        return redirect()->route('student.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) //get post put patch delete
    {        
        function escape_like_str($str){
            $like_escape_char = '!';

            return str_replace([$like_escape_char, '%', '_'], [
                $like_escape_char.$like_escape_char,
                $like_escape_char.'%',
                $like_escape_char.'_',
            ], $str);
        }

        $all=array();
        $all['current']=isset($request->page)?$request->page:1;
        $all['studentOR']=DB::table('student_departments')
                        ->join('students', 'student_departments.Snum', '=', 'students.Snum')
                        ->where('student_departments.login','2')
                        ->select('students.*', 'student_departments.department');
        if(session()->get('status')=="office"){
            $department= session()->get('department');   
            if(isset($request->search)){
                $search=$request->search;
                $all['student']=$all['studentOR']
                        ->where('students.Sname','like',"%".escape_like_str($search)."%")
                        ->where('student_departments.department',$department)
                        ->offset(isset($request->page)?($request->page-1)*50:0)->limit(50)
                        ->get();
                $all['total']=$all['studentOR']->count();
            }else{
                $all['student']=$all['studentOR']
                        ->where('student_departments.department',$department)
                        ->offset(isset($request->page)?($request->page-1)*50:0)->limit(50)->get();
                $all['total']=$all['studentOR']
                        ->where('student_departments.department',$department)->count();
            }
        }else if(session()->get('status')=="IR"){
            $department= "";
            if(isset($request->search)){
                $search=$request->search;
                    $all['student']=$all['studentOR']
                    ->where('students.Sname','like',"%".escape_like_str($search)."%")
                    ->offset(isset($request->page)?($request->page-1)*50:0)->limit(50)->get();
                    
                    $all['total']=$all['studentOR']->where('students.Sname','like',"%".escape_like_str($search)."%")->count();
            }else{
                 $all['student']=$all['studentOR']->offset(isset($request->page)?($request->page-1)*50:0)->limit(50)->get();
                 $all['total']=$all['studentOR']->count();
            }
            
        }else{
            return json_encode([]);
        }
        $all['studentOR']=[];
        $all['next']=$all['current']+1;
        

        // $data=DB::table('students')->offset(isset($request->page)?($request->page-1)*50:0)->limit(50)->get();                                
        return urldecode(json_encode($all)); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $back = $_SERVER["HTTP_REFERER"];
        if (isset($request->Snum)) {
            $data1['Snum'] = $request->Snum;
        }
        if (isset($request->Sname)) {
            $data1['Sname'] = $request->Sname;
        }
        if (isset($request->income)) {
            $data1['income'] = $request->income;
        }
        if (isset($request->identifity)) {
            $data1['identifity'] = $request->identifity;
        }
        if (isset($request->graduation)) {
            $data1['graduation'] = $request->graduation;
        }
        if (isset($request->category)) {
            $data1['category'] = $request->category;
        }
        if (isset($request->gender)) {
            $data1['gender'] = $request->gender;
        }
        if (isset($request->schoolnum)) {
            $data1['schoolnum'] = $request->schoolnum;
        }
        if (isset($request->year)) {
            $data1['year'] = $request->year;
        }
        if (isset($request->address)) {
            $data1['address'] = $request->address;
        }
        if(Student::where('Snum',$request->Snum)->where('id','!=', $request->id)->exists()){
            Cache::put('datastatus', '應試號碼重複出現' , 3);
        }else{
            Cache::put('datastatus', '修改成功' , 3);
        }
        if(isset($data1)){
            $o=Student::where('id', $request->id)
                ->update($data1);
        }
        if (isset($request->department) && isset($request->olddepartment)) {
            $olddepartment = $request->olddepartment;
            $Snum=Student::where('id', $request->id)->value('Snum');
            $sdid=StudentDepartment::where('Snum',$Snum)->where('department',$olddepartment)->where('login','2')->value('id');
            StudentDepartment::where('id',$sdid)->update(['department' => $request->department]);
        }
        return redirect()->route('student.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        // dd($request->id);
        $Snum = DB::table('students')->where('id', $request->id)->value('Snum');
        DB::table('students')->where('id', $request->id)->delete();
        Cache::put('datastatus', '刪除成功' , 3);
        return redirect()->route('student.index');
    }


}
