<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Flight;
use \Cache;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToArray;
use function MongoDB\BSON\toJSON;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;

class GiveOfficeLoginController extends Controller
{

    public function setCookie()
    {
        dd("123");
        return response('Hello Cookie')->cookie('test', '123', 60);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->get('status')=="IR") {
            //cookie="bwerqer123123"
            return response(view('IR.index'))->cookie('key', bcrypt(session()->get('account')), 120, null, null, false, false);
        } else {
            setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
            $url = "https://sso.nuu.edu.tw/api/logout.php";
            $data_array = array("account" => session()->get('account'));
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data_array)
                )
            );
            $context  = stream_context_create($options);
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $data = $request->all();
        $datas = array_except($data, ['_token']);
        // dd($datas);
        $datas['login'] = 'office';
        DB::table('flights')->insert($datas);
        Cache::put('datastatus', '儲存成功' , 3);
        return redirect()->route('flight.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //axios
        // $keys = DB::table("flights")->select('*')->where('login', '=', "IR")->pluck("Faccount");
        // if ($request->cookie('key') != "") {
        //     // $request->cookie('key')
        //     foreach ($keys as $key => $value) {
        //         if (Hash::check($value, $request->cookie('key'))) {
                    // The passwords match...
        if(session()->get('status')=='IR'){
            $users = DB::table('flights')->where('login', '=', 'office')->get();
            return json_encode($users);
        }
        //         }
        //     }
        // }
        return json_encode([]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        // dd($request->department);
        if (isset($request->Faccount)) {
            $datas['Faccount'] = $request->Faccount;
        }
        if (isset($request->Fname)) {
            $datas['Fname'] = $request->Fname;
        }
        if (isset($request->department)) {
            $datas['department'] = $request->department;
        }
        if (isset($request->date)) {
            $datas['date'] = $request->date;
        }

        DB::table('flights')
            ->where('id', $request->id)
            ->update($datas);
        Cache::put('datastatus', '更新成功' , 3);
        return redirect()->route('flight.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //

        $flight = DB::table('flights')->where('id', $request->id)->delete();
        Cache::put('datastatus', '刪除成功' , 3);
        return redirect()->route('flight.index');
    }
}
