<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use function MongoDB\BSON\toJSON;
use App\Flight;
use App\Teacher;
use \Cache;
class SelectIdentifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if(session()->get('account')!=""){
            return view('Selectidentify');
        }else{
            setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
            $url = "https://sso.nuu.edu.tw/api/logout.php";
            $data_array = array("account" => session()->get('account'));
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data_array)
                )
            );
            $context  = stream_context_create($options);
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(session()->get('account')!="" ){
            $data = Flight::where('Faccount',session()->get('account'))->select("id",'Faccount','Fname','department','login')->get();
            foreach ($data as $key => $value) {
                # code...
                switch ($value['login']) {
                    case 'IR':
                        $value['login']="管理者";
                        break;
                    case 'Admin':
                        $value['login']="維修人員";
                        break;
                    case 'head':
                        $value['login']="主任";
                        break;
                    case 'teacher':
                        // $temp = Teacher::where('Taccount','=',session()->get('account'))->where('login','=','2')->select('*')->distinct()->get(); //取不重複的正確身分(未被刪除的)
                        if (Teacher::where('Taccount',$value['Faccount'])->where('department',$value['department'])->where('login','=','2')->exists()){
                            $value['login']="書審委員";
                        }else{
                            $value['login']="deleted";
                            $data->forget($key); //刪除被軟刪除過的資料
                        }

                        break;
                    case 'office':
                        $value['login']="系辦助理";
                        break;
                }


            }
            return urldecode(json_encode($data));
        }
        return json_encode([]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        function getID($array,$id){
            foreach($array as $item){
                if($item==$id){
                    return true;
                }
            }
            return false;
        }
        $arrayId=Flight::where('Faccount',session()->get('account'))->pluck('id');
        $arrayId=$arrayId->toArray();
        if(!getID($arrayId,$request->id)){
            return redirect('/logout');
        }
        session()->put('status',  Flight::where('id',$request->id)->value('login'));
        session()->put('department', Flight::where('id',$request->id)->value('department'));
        switch (session()->get('status')) {
            case 'IR':
                return redirect('/student');
            case 'Admin':
                return redirect('/admin');
            case 'head':
                return redirect('/detail');
            case 'teacher':
                return redirect('/judge');
            case 'office':
                return redirect('/head');
            default:
                return  redirect('Undefined');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
