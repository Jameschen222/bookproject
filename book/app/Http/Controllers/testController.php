<?php

namespace App\Http\Controllers;
use App\MarkData;
use App\Student;
use App\Grade;
use App\Match;
use App\Teacher;
use App\StudentDepartment;
use \Cache;
use App\MatchNum;
use Illuminate\Http\Request;

class testController extends Controller
{
    //
    public function index(Excelcontroll $excelcontroll)
    {
        //
    }
    public function show(Request $request){
        if (session()->get('status') == "Admin" || session()->get('status') == "office" ) {
            //cookie="bwerqer123123"
            return response(view('test'));
            
        } else {
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
            
        
    }
    public function gradedestroy(Request $request)
    {
        //刪除系所成績
        if (session()->get('status') == "Admin"  ) {
            // dd($request,$request->department,Grade::where('department',$request->department)->where('login','2')->get(),Grade::where('department',$request->department)->get());
            Grade::where('department',$request->department)->delete();
            return view('test');
        }else {
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
    }
    public function jsupdate(Request $request)
    {
        //
        // dd($request->all());
        $department = session()->get('department');
        MarkData::where('department', $department)->where('login','2')->delete();
        Grade::where('department', $department)->where('login','2')->delete();
        if(isset($request->project)){
            $data=MarkData::where('department', $department)->where('login',$request->project)->get();
            $insert_data=[];
            $Bitem=array();
            $Mitem=array();
            $Sitem=array();

            foreach ($data as $key => $value) {
                if($value['title']=="")dd($value);
                $insert_data= array(
                    'Bitem' => $value['Bitem'],
                    'Mitem' => $value['Mitem'],
                    'Sitem' => $value['Sitem'],
                    'title' => $value['title'],
                    'HighScore' => $value['HighScore'],
                    'lowScore' => $value['lowScore'],
                    'login' => '2',
                    'project' => $request->newproject,
                    'department' => $department,
                    'percent' => $value['percent'],
                );
                MarkData::insert($insert_data);
                // dump($insert_data);
                array_push($Bitem,$value['Bitem']);
                array_push($Mitem,$value['Mitem']);
                array_push($Sitem,$value['Sitem']);
            }
        }
        $count = count($Bitem);
        // dump($Bitem,$Mitem,$Sitem);
        for ($a = 0; $a < $count; $a++) {
            //預設評分
            // dump('in');
            if ($Sitem[$a] == "0") {
                $Snum = StudentDepartment::where("department", $department)->where('login', '2')->pluck('Snum');
                // dump($Snum);
                foreach ($Snum as $b) {                   
                    $Gradedata['Snum'] = $b;
                    $Gradedata['department'] = $department;
                    $Gradedata['Bitem'] = $Bitem[$a];
                    $Gradedata['Mitem'] = $Mitem[$a];
                    $Gradedata['login'] = 2;
                    $Gradedata['score'] = 0;
                    $Gradedata['block'] = -1;
                    $Taccount = Match::where("department", $department)->where("Snum", $b)->where("choose", "1")->where('login', '2')->pluck('Taccount');
                    // dump($Taccount);
                    foreach ($Taccount as $c) {
                        $Gradedata['Taccount'] = $c;
                        dump($Gradedata);
                        Grade::insert($Gradedata);
                    }
                }
            }
        }
        // dd();
        return back();
    }
    public function reupdate(Request $request)
    {
        //
        // $department="經營管理學系";
        // StudentDepartment::where('department',$department)->update(['login'=>'2']);
        // Teacher::where('department',$department)->update(['login'=>'2']);
        // Match::where('department',$department)->update(['login'=>'2']);
        // MatchNum::where('id',15)->update(['login'=>'2']);
        // MatchNum::where('department',$department)->update(['login'=>'2']);
        // Grade::where('department',$department)->update(['login'=>'2']);
        // dd(MarkData::where('department','土木與防災工程學系')->where('login','109test')->get());
        // MarkData::whereIn('id', [831,832])->delete();
        // MarkData::whereIn('id', [831,832])->delete();
        // MarkData::where('department','土木與防災工程學系')->where('login','!=','2')->delete();
        // dd(MarkData::where('department','土木與防災工程學系')->where('login','!=','2')->get());
        // MarkData::where('department','土木與防災工程學系')->where('project','234')->update(['login'=>'2']);
        
        
    }
    public function showteacherstandard(Request $request)
    {
        //秀老師標轉差
        if (session()->get('status') == "IR"  ) {
            if(isset($request->department)&&isset($request->project)){
                $data=[];
                $teachersta=[];
                $teacher=Match::where('department',$request->department)->where('login',$request->project)->select('Taccount')->distinct()->get();
                $temdata=[];
                $z=-1;
                foreach( $a as $teacher){
                    $student=Match::where('department',$request->department)->where('login',$request->project)->where('Taccount',$a)->select('Snum');
                    $ave=( Match::where('department',$request->department)->where('login',$request->project)
                        ->where('Taccount',$a)->select('score')->sum() ) / count($student);
                }
            }else if(isset($request->department)){
                $data=[];
                $data['project']=Standard::where('department',$request->department)->select('project')->distinct()->get();
                $data['department']=$request->department;
                return json_encode($data);
            }else{
                return Department::get();
            }
            return view('');
        }else {
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
    }
}
