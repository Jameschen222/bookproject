<?php

namespace App\Http\Controllers;

use App\Exports\exportStandardDeviation;
use App\Imports\TeacherJudgeImport;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Expr\Cast\Object_;
use PhpParser\Node\Scalar\String_;

class importAndShowGrades extends Controller
{
    private $insert_data;

    public function index(){
        if (session()->get('status')=="IR") {
            return response(view('IR.standard'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
        }
        switch (session()->get('status')) {
            case 'Admin':
                return redirect('/admin');
            case 'head':
                return redirect('/detail');
            case 'teacher':
                return redirect('/judge');
            case 'office':
                return redirect('/head');
            default:
                return  redirect('Undefined');
        }
        setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
        $url = "https://sso.nuu.edu.tw/api/logout.php";
        $data_array = array("account" => session()->get('account'));
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data_array)
            )
        );
        $context  = stream_context_create($options);
        session()->flush();
        session()->put('error', '權限不足 請重新登入');
        return view('welcome');
    }

    public function show(Request $request){
        if (session()->get('status') == 'IR' or session()->get('status') == 'head'){
            if ($request->department == null) {
                $department = DB::table('teacher_standard_deviation')->select('department')->distinct()->get();
                return json_encode($department);
            }else{
                $department = $request->department;
                //DB::table('teacher_standard_deviation')->where('Taccount','名額類別')->where('department','華文系')->delete();
//        $ss = DB::table('teacher_standard_deviation')->select('*')->where('Taccount','KIWI')->where('department','化工系')->get();
//        dd($ss);
                $showdata=[];
                $tempstu = [];
                $standarddeviation=[];
                $insert_data = [];

                $teachernum = DB::table('teacher_standard_deviation')->select('Taccount')->where('department',$department)->distinct()->get();
                $stunum = DB::table('teacher_standard_deviation')->select('Snum')->where('department',$department)->distinct()->get();
                $show = DB::table('teacher_standard_deviation')->select('*')->where('department',$department)->get();
                if (isset($teachernum[0]) and isset($stunum[0]) and isset($show[0])) {
                    $numteacher = count($teachernum);
                    $numstu = count($stunum);
                    $numshow = count($show);

                    for ($x = 0; $x < $numteacher; $x++) {
                        $allscore = 0;
                        for ($y = 0; $y < $numstu; $y++) {
                            $totalscore = 0;
                            $tagetstuscore[$y] = 0;
                            for ($z = 0; $z < $numshow; $z++) {
                                if ($show[$z]->Taccount == $teachernum[$x]->Taccount and $show[$z]->Snum == $stunum[$y]->Snum) {
                                    $totalscore += $show[$z]->score;  //加總每一位學生的分數
                                }
                            }
                            $allscore += $totalscore;
                            $tagetstuscore[$y] = $totalscore;
                        }
                        $averge = (float)$allscore / $numstu;
                        $temp = 0;
                        for ($y = 0; $y < $numstu; $y++) {
                            $temp += (float)pow($tagetstuscore[$y] - $averge, 2);
                        }
                        $standarddeviation[$x] = (Object)array(
                            'Taccount' => $teachernum[$x]->Taccount,
                            'standarddeviation' => number_format(sqrt($temp / $numstu), 2, '.', ''),
                        );
                    }
                    $temp = [];
                    unset($temp);
                    $numSD = count($standarddeviation);
                    for ($x = 0; $x < $numSD; $x++) {
                        for ($y = 0; $y < $numstu; $y++) {
                            for ($z = 0, $count = 0; $z < $numshow; $z++) {
                                if ($show[$z]->Taccount == $standarddeviation[$x]->Taccount and $show[$z]->Snum == $stunum[$y]->Snum) {
                                    $temp[$count++] = (Object)array(
                                        "Bitem" => $show[$z]->Bitem,
                                        "score" => $show[$z]->score
                                    );
                                }
                            }
                            $check = 0;
                            for ($z = 0; $z < $numshow; $z++) {
                                if ($show[$z]->Taccount == $standarddeviation[$x]->Taccount and $show[$z]->Snum == $stunum[$y]->Snum and $check == 0) {
                                    $check = 1;
                                    $tempstu[$y] = (Object)array(
                                        'id' => $y + 1,
                                        'Taccount' => $standarddeviation[$x]->Taccount,
                                        'department' => $show[$z]->department,
                                        'reviewClass' => $show[$z]->reviewClass,
                                        'quotaClass' => $show[$z]->quotaClass,
                                        'Snum' => $show[$z]->Snum,
                                        'name' => $show[$z]->name,
                                        'sex' => $show[$z]->sex,
                                        'scores' => $temp,
                                        'Remark' => $show[$z]->Remark
                                    );
                                }
                            }
                        }
                        $standarddeviation[$x] = (Object)array(
                            'Taccount' => $standarddeviation[$x]->Taccount,
                            'standarddeviation' => $standarddeviation[$x]->standarddeviation,
                            'judgescores' => $tempstu,
                        );
                        array_push($showdata,$standarddeviation[$x]);
                    }

//            $check = DB::table('final_standarddeviation')->select('*')->get();
//            $check = $check->toArray();
//            for ($x = 0;$x<count($check);$x++){
//                $check[$x] = (array)$check[$x];
//            }
//            foreach ($check as $key => $subArr){
//                unset($subArr['id']);
//                unset($subArr['created_at']);
//                unset($subArr['updated_at']);
//                $check[$key] = $subArr;
//            }

                    if (!isset($check[0])) {
                        for ($x = 0; $x < count($standarddeviation); $x++) {
                            DB::table('final_standarddeviation')->updateOrInsert(['Taccount' => $standarddeviation[$x]->Taccount], ['department' => $standarddeviation[$x]->judgescores[0]->department, 'StandardDeviation' => $standarddeviation[$x]->standarddeviation]);
                        }
                    } else {
                        for ($x = 0; $x < count($standarddeviation); $x++) {
                            $insert_data[$x] = array(
                                'Taccount' => $standarddeviation[$x]->Taccount,
                                'department' => $standarddeviation[$x]->judgescores[0]->department,
                                'StandardDeviation' => $standarddeviation[$x]->standarddeviation,
                            );
                        }
                        //dd($check,$insert_data);
                        DB::table('final_standarddeviation')->delete();
                        DB::table('final_standarddeviation')->insert($insert_data);
                    }
                } else {
                    cache()->put('datastatus', '資料庫尚未有資料', 3);
                    return [];
                }
                return json_encode($showdata);
            }
        }else{
            $department = session()->get('department');
            //DB::table('teacher_standard_deviation')->where('Taccount','名額類別')->where('department','華文系')->delete();
//        $ss = DB::table('teacher_standard_deviation')->select('*')->where('Taccount','KIWI')->where('department','化工系')->get();
//        dd($ss);
            $showdata=[];
            $tempstu = [];
            $standarddeviation=[];
            $insert_data = [];

            $teachernum = DB::table('teacher_standard_deviation')->select('Taccount')->where('department',$department)->distinct()->get();
            $stunum = DB::table('teacher_standard_deviation')->select('Snum')->where('department',$department)->distinct()->get();
            $show = DB::table('teacher_standard_deviation')->select('*')->where('department',$department)->get();
            if (isset($teachernum[0]) and isset($stunum[0]) and isset($show[0])) {
                $numteacher = count($teachernum);
                $numstu = count($stunum);
                $numshow = count($show);

                for ($x = 0; $x < $numteacher; $x++) {
                    $allscore = 0;
                    for ($y = 0; $y < $numstu; $y++) {
                        $totalscore = 0;
                        $tagetstuscore[$y] = 0;
                        for ($z = 0; $z < $numshow; $z++) {
                            if ($show[$z]->Taccount == $teachernum[$x]->Taccount and $show[$z]->Snum == $stunum[$y]->Snum) {
                                $totalscore += $show[$z]->score;  //加總每一位學生的分數
                            }
                        }
                        $allscore += $totalscore;
                        $tagetstuscore[$y] = $totalscore;
                    }
                    $averge = (float)$allscore / $numstu;
                    $temp = 0;
                    for ($y = 0; $y < $numstu; $y++) {
                        $temp += (float)pow($tagetstuscore[$y] - $averge, 2);
                    }
                    $standarddeviation[$x] = (Object)array(
                        'Taccount' => $teachernum[$x]->Taccount,
                        'standarddeviation' => number_format(sqrt($temp / $numstu), 2, '.', ''),
                    );
                }
                $temp = [];
                unset($temp);
                $numSD = count($standarddeviation);
                for ($x = 0; $x < $numSD; $x++) {
                    for ($y = 0; $y < $numstu; $y++) {
                        for ($z = 0, $count = 0; $z < $numshow; $z++) {
                            if ($show[$z]->Taccount == $standarddeviation[$x]->Taccount and $show[$z]->Snum == $stunum[$y]->Snum) {
                                $temp[$count++] = (Object)array(
                                    "Bitem" => $show[$z]->Bitem,
                                    "score" => $show[$z]->score
                                );
                            }
                        }
                        $check = 0;
                        for ($z = 0; $z < $numshow; $z++) {
                            if ($show[$z]->Taccount == $standarddeviation[$x]->Taccount and $show[$z]->Snum == $stunum[$y]->Snum and $check == 0) {
                                $check = 1;
                                $tempstu[$y] = (Object)array(
                                    'id' => $y + 1,
                                    'Taccount' => $standarddeviation[$x]->Taccount,
                                    'department' => $show[$z]->department,
                                    'reviewClass' => $show[$z]->reviewClass,
                                    'quotaClass' => $show[$z]->quotaClass,
                                    'Snum' => $show[$z]->Snum,
                                    'name' => $show[$z]->name,
                                    'sex' => $show[$z]->sex,
                                    'scores' => $temp,
                                    'Remark' => $show[$z]->Remark
                                );
                            }
                        }
                    }
                    $standarddeviation[$x] = (Object)array(
                        'Taccount' => $standarddeviation[$x]->Taccount,
                        'standarddeviation' => $standarddeviation[$x]->standarddeviation,
                        'judgescores' => $tempstu,
                    );
                    array_push($showdata,$standarddeviation[$x]);
                }

//            $check = DB::table('final_standarddeviation')->select('*')->get();
//            $check = $check->toArray();
//            for ($x = 0;$x<count($check);$x++){
//                $check[$x] = (array)$check[$x];
//            }
//            foreach ($check as $key => $subArr){
//                unset($subArr['id']);
//                unset($subArr['created_at']);
//                unset($subArr['updated_at']);
//                $check[$key] = $subArr;
//            }

                if (!isset($check[0])) {
                    for ($x = 0; $x < count($standarddeviation); $x++) {
                        DB::table('final_standarddeviation')->updateOrInsert(['Taccount' => $standarddeviation[$x]->Taccount], ['department' => $standarddeviation[$x]->judgescores[0]->department, 'StandardDeviation' => $standarddeviation[$x]->standarddeviation]);
                    }
                } else {
                    for ($x = 0; $x < count($standarddeviation); $x++) {
                        $insert_data[$x] = array(
                            'Taccount' => $standarddeviation[$x]->Taccount,
                            'department' => $standarddeviation[$x]->judgescores[0]->department,
                            'StandardDeviation' => $standarddeviation[$x]->standarddeviation,
                        );
                    }
                    //dd($check,$insert_data);
                    DB::table('final_standarddeviation')->delete();
                    DB::table('final_standarddeviation')->insert($insert_data);
                }
            } else {
                cache()->put('datastatus', '資料庫尚未有資料', 3);
                return [];
            }
            return json_encode($showdata);
        }


    }

    public function importTeacherGrades(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx,csv'
        ]);
        $path = $request->file('select_file');
        $data = Excel::toCollection(new TeacherJudgeImport(), $path)->toArray();
        if (isset($request->department) and isset($request->BitemNum)) {
            $numforcheck = $request->BitemNum+8;
            if ($numforcheck!= count($data[0][0])){
                cache()->put('datastatus', '資料有誤，請檢察格式', 3);
                unset($data,$path,$showData,$numforcheck);
                return back();
            }else {
                for ($x = 0; $x < count($data); $x++) {
                    $count = 0;
                    for ($y = 1; $y < count($data[$x]); $y++) {
                        for ($z = 0; $z < $request->BitemNum; $z++) {
                            $showData = array(
                                "reviewClass" => $data[$x][$y][4],
                                "quotaClass" => $data[$x][$y][0],
                                "Snum" => $data[$x][$y][1],
                                "name" => $data[$x][$y][2],
                                "sex" => $data[$x][$y][3],
                                "Taccount" => $data[$x][0][0],
                                "Bitem" => str_replace("\n", "", $data[$x][0][$z + 6]),
                                "score" => (string)$data[$x][$y][$z + 6],
                                "department" => $request->department,
                                "Remark" => str_replace("\n", "<br>", $data[$x][$y][count($data[$x][$y]) - 1])
                            );
                            $this->insert_data[$x][$count++] = $showData;
                        }
                    }
                }
            }
        }
        try {
            for ($x = 0; $x < count($data); $x++) {
                DB::table('teacher_standard_deviation')->insert($this->insert_data[$x]);
            }
            cache()->put('datastatus', '新增成功', 3);
            unset($data,$path,$showData,$numforcheck,$this->insert_data);
            return back();
        } catch (QueryException $exception) {
            if ($exception->getCode() == "23000") {
                cache()->put('datastatus', '資料庫無法新增重複資料！', 3);
                unset($data,$path,$showData,$numforcheck,$this->insert_data);
                return back();
            } else {
                cache()->put('datastatus', '錯誤，錯誤代碼:' . $exception->getCode() . '，請聯絡維修人員', 3);
                unset($data,$path,$showData,$numforcheck,$this->insert_data);
                return back();
            }
        }
    }

    public function showalldepartment(Request $request){
            $department = DB::table('student_departments')->select('department')->distinct()->get();
            return json_encode($department);
    }

    public function export(){
        $datetime = date("Y_m _d");
        return Excel::download(new exportStandardDeviation(),$datetime . '所有系所老師評分標準差.xlsx');
    }
}
