<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Grade extends Model
{
    //
    protected $table = "grades";
    protected $fillable = [
        'Bitem', 'Mitem', 'Snum', 'department', 'score', 'Taccoount', 'date','login'
    ];
    public static function del($department)
    {

        $student = DB::table('grades')->where('department', $department)->delete();
        return $student;
    }
}
