<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (isset($_COOKIE['token']) || session()->get('account')!="") {
        if(session()->get('account')!=""){
            switch (session()->get('status')) {
                case 'IR':
                    return redirect('/student');
                case 'Admin':
                    return redirect('/admin');
                case 'head':
                    return redirect('/detail');
                case 'teacher':
                    return redirect('/judge');
                case 'office':
                    return redirect('/head');
                default:
                    return  redirect('Undefined');
            }
        }
        $url = "https://sso.nuu.edu.tw/api/checkToken.php";
        $data_array = array("token" => $_COOKIE['token'], "system_name" => "書審輔助系統");
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data_array)
            )
        );
        $context  = stream_context_create($options);
        $json = file_get_contents($url, false, $context);
        //print($json);

        $json = json_decode($json);
        $data["Faccount"]=$json->account;
        $data["Fname"]=$json->name;
        $account = DB::table('flights')->where('Faccount', '=', $json->account)->get();

        if ($account->count() > 0) {
            DB::table('flights')
            ->where('id', $account[0]->id)
            ->update($data);
            $account = DB::table('flights')->where('Faccount', '=', $json->account)->get();
            session()->flush();
            session()->put('status',  $account[0]->login);
            session()->put('account',  $account[0]->Faccount);
            session()->put('name',  $account[0]->Fname);
            session()->put('department',  $account[0]->department);
            session()->put('position',  $account[0]->position);
            $datetime = date ("Y-m-d H:i:s",mktime(date('H')+8, date('i')+15, date('s'), date('m'), date('d'), date('Y')));
            session()->put('availableTime', $datetime);
            //dd(session()->get('availableTime'));
            return redirect('/SelectIdentify');
        }
        return  redirect('Undefined');

    }

    return view('welcome');
});
Route::get('/excel', function () {
    return view('exceltest');
});
Route::get('Undefined', function () {
    return view('Undefined');
});
    Route::get('/fetchdatafortimes', 'ExcelController@show')->name('timesfetch');

    Route::get('/test', function () {
        return view('IR/index');
    });



    Route::get('/TimeRequest', 'loginTimeController@show');
    Route::get('/TimeRequestReset', 'loginTimeController@resetTime');


    /*匯入系辦權限*/
    Route::post('Exceltest/importflightsDepartOffice', 'ExcelController@importflightsDepartOffice')->name('iptOfDepartment');

    /*匯入主任權限*/
    Route::post('Exceltest/importflightsDepartHead', 'ExcelController@importflightsDepartHead')->name('iptOfDepartHead');

    /*匯入學生資料*/
    Route::post('excel/importstudents', 'ExcelController@importstudents')->name('iptOfStudents');

    /*匯入老師權限*/
    Route::post('/importflightsDepartTeacher', 'ExcelController@importflightsDepartTeacher')->name('iptOfTeacheraccount');
    /*更新老師權限*/
    Route::post('excel1/updateflightsDepartTeacher', 'ExcelController@updateflightsDepartTeacher')->name('uptOfTeacheraccount');

    /*匯入配對資料*/
    Route::post('excel1/importmatches', 'ExcelController@importmatches')->name('iptOfMatches');

    /*更新主任權限*/
    Route::post('excel1/updateflightsDepartHead', 'ExcelController@updateflightsDepartHead')->name('uptOfDepartHead');

    /*更新學生資料*/
    Route::post('excel1/updatestudents', 'ExcelController@updatestudents')->name('uptOfStudents');

    /*IR更新系辦權限*/
    Route::post('excel1/IRupdate1', 'IR_ExcelController@updateflightsDepartOffice_FORIR')->name('IRDOupdate');

    /*配對匯入*/
    Route::post('excel/matchimport', 'ExcelController@matchesImporter')->name('matchesImporter');

    /*匯入尺規*/
    Route::post('excel/stepsimport', 'ExcelController@stepsImporter')->name('stepsImporter');

    Route::post('excel/importTeacherJudgement', 'importAndShowGrades@importTeacherGrades')->name('importGrades');

    Route::get('excel/show', 'importAndShowGrades@show')->name('show');

    Route::get('excel/export', 'ExcelController@expotexcel')->name('export');

    Route::get('excel/exportonlysteps', 'ExcelController@exportonlysteps')->name('exportonlysteps');//會出面向(沒有成績的)

    Route::get('excel/exportn', 'ExcelController@exportnewteacherdata')->name('exportTNewdata');//匯出新老師資料

    Route::get('excel/exportne', 'ExcelController@exportnewheaddata')->name('exportHNewdata');//匯出新主任資料

    Route::get('excel/exportnew', 'ExcelController@exportnewofficedata')->name('exportONewdata');//匯出新系辦資料

    Route::get('excel/exportnewS', 'ExcelController@exportnewStudentdata')->name('exportSNewdata');//匯出新學生資料

    Route::get('excel/exportForStandardDeviation', 'importAndShowGrades@export')->name('exportForSD');//匯出標準差

    Route::get('compare', 'compareController@show')->name('compareshow');

    Route::get('getalldepartment', 'importAndShowGrades@showalldepartment')->name('showalldepartment');

Route::resource('/admin', "admin");
Route::get('logout', function () {
    if (isset($_COOKIE['token'])) {
        setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
        $url = "https://sso.nuu.edu.tw/api/logout.php";
        $data_array = array("account" => session()->get('account'));
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data_array)
            )
        );
        $context  = stream_context_create($options);
    }
    session()->flush();
    return view('welcome');
});
Route::get('judge2',function(){
    return view("Teacher.index2");
});

Route::resource('/flight', "GiveOfficeLoginController"); // IR
Route::resource('/head', "GiveheadLoginController"); // 系辦
Route::resource('/student', "StudentController");   // 系辦
Route::resource('/teacher', "TeacherController"); // 系辦
Route::resource('/match', "MatchController");   // 系辦
Route::resource('/departments', "DepartmentController");  //不用做
Route::resource('/standard', "JudgeStandardController"); // 系辦
Route::resource('/detail', "GradeDetailController");
Route::resource('/judge', "JudgeController");  //老師
Route::resource('/ForIR', "ForIR");  //admin
Route::resource('/delete', "OfficeDeleteController");  //admin  軟刪除
Route::resource('/Adjust', "AdjustmentScoreController");  //IR
Route::get('/test', 'testController@show');//匯出新主任資料
Route::post('/test/gradedestroy', 'testController@gradedestroy')->name('dedepartemntgrade');//刪除系所成績
Route::post('/test/upstandard', 'testController@jsupdate')->name('jsupdate');//測試
Route::post('/test/reupdate', 'testController@reupdate')->name('reupdate');//測試
Route::get('migrateexecute','admin@update')->name('migrate');
Route::get('/teacherave', 'testController@showteacherstandard');//老師標準差

// Route::post('excel1/updateflightsDepartHead', 'ExcelController@updateflightsDepartHead')->name('uptOfDepartHead');


Route::get('/Allgrade',function(){ //excel
    if(session()->get('status')=="IR"){
        return view("IR.allDepartmentScore");
    }
    return redirect('/logout');
});
Route::get('/ForWith',function(){
    Cache::put('datastatus', 'Error Message Here123' , 3);
    return redirect('match');
});
Route::get('/IRcompares',function(){
    if(session()->get('status')=="IR"){
        return view("IR.compares");
    }
    return redirect('/logout');
});

// Route::resource('/headupdatedrade', "OfficeDeleteControlle@show");
// Route::resource('/list', "OfficeDeleteControlle@show");
// Route::resource('/difference', "OfficeDeleteControlle@show");
Route::resource('/SelectIdentify', "SelectIdentifyController");  //選身分
Route::resource('/deleteteacher','DeleteTeacherController');

Route::get('/adminTable',function(){
    switch (session()->get('status')) {
        case 'IR':
            return redirect('/student');
        case 'Admin':
            return view('admin/DB');
        case 'head':
            return redirect('/detail');
        case 'teacher':
            return redirect('/judge');
        case 'office':
            return redirect('/head');
        default:
            return redirect('Undefined');
    }
});

Route::get('/Score_range', function () {
    return view('Department/Score_range');
});

Route::get('/test2', function () {
    return view('Department.All_scores');
});
Route::get('/Differential_checklist', function () {
    return view('Differential checklist');
});

Route::get('/DepartmemtCompare', function () {
    switch (session()->get('status')) {
        case 'IR':
            return redirect('/student');
        case 'Admin':
            return view('admin/DB');
        case 'head':
            return view('Department/compares');
        case 'teacher':
            return redirect('/judge');
        case 'office':
            return view('Department/compares');
        default:
            return redirect('Undefined');
    }
});

Route::resource('/importjudgedata', "importAndShowGrades");

Route::get('/reset', function () {
    if (isset($_COOKIE['token'])) {
        $url = "https://sso.nuu.edu.tw/api/checkToken.php";
        $data_array = array("token" => $_COOKIE['token'], "system_name" => "書審輔助系統");
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data_array)
            )
        );
        $context  = stream_context_create($options);
        $json = file_get_contents($url, false, $context);
        //print($json);

        $json = json_decode($json);
        $account = DB::table('flights')->where('Faccount', '=', $json->account)->get();
        if ($account->count() > 0) {
            session()->put('status',  $account[0]->login);
            session()->put('account',  $account[0]->Faccount);
            session()->put('name',  $account[0]->Fname);
            session()->put('department',  $account[0]->department);
            session()->put('position',  $account[0]->position);
            switch (session()->get('status')) {
                case 'IR':
                    return redirect('/student');
                case 'Admin':
                    return redirect('/admin');
                case 'head':
                    return redirect('/detail');
                case 'teacher':
                    return redirect('/judge');
                case 'office':
                    return redirect('/head');
            }
        }
        return  redirect('Undefined');
    } else {
        if(session()->get('Oldaccount')!=""){
            $account = DB::table('flights')->where('Faccount', '=', session()->get('Oldaccount'))->get();
            session()->flush();
            session()->put('status',  $account[0]->login);
            session()->put('account',  $account[0]->Faccount);
            session()->put('name',  $account[0]->Fname);
            session()->put('department',  $account[0]->department);
            session()->put('position',  $account[0]->position);
            $datetime = date ("Y-m-d H:i:s",mktime(date('H')+8, date('i')+15, date('s'), date('m'), date('d'), date('Y')));
            session()->put('availableTime', $datetime);
            //dd(session()->get('availableTime'));
            return redirect('/SelectIdentify');
        }
        return view('welcome');
    }
});
