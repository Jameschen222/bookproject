<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjustScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjust_scores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('Snum');
            $table->String('department');
            $table->String('OragScore')->nullable();;
            $table->float('StudentAve')->nullable();;
            $table->float('TeacherAve')->nullable();;
            $table->float('TeacherSta')->nullable();;
            $table->float('WantAve')->nullable();;
            $table->float('WantSta')->nullable();;
            $table->float('TeacherZ')->nullable();;
            $table->String('AdjustScore')->nullable();;
            $table->String('AdjustAve')->nullable();;
            $table->float('ZRMax')->nullable();;
            $table->float('ZRMin')->nullable();;
            $table->float('dZR')->nullable();;
            $table->float('Level')->nullable();;
            $table->float('Coefficient')->nullable();;
            $table->float('PRGap')->nullable();;
            $table->string("login");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjust_scores');
    }
}
