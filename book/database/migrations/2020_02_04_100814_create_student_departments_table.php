<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_departments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Snum');
            $table->string("department"); //全部
            $table->string("departmentid"); //全部
            $table->foreign("Snum")->references('Snum')->on('students')->onUpdate('cascade')->onDelete('cascade');
            $table->string("login");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_departments');
    }
}
