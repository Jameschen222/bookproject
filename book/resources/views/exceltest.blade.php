<button type="button" onclick="location.href='{{route('migrate')}}'" >執行</button>
<h1>{{session()->get('error')}}</h1>
<form action="{{route('iptOfDepartment')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="file" name="select_file">
    <input type="submit" value="系辦匯入測試">
</form>
<form action="{{route('iptOfDepartHead')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="file" name="select_file">
    <input type="submit" value="主任匯入測試">
</form>
<form action="{{route('iptOfStudents')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="file" name="select_file">
    <input type="submit" value="學生資料匯入測試">
</form>
<form action="{{route('iptOfTeacheraccount')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="file" name="select_file">
    <input type="submit" value="老師權限匯入測試">
</form>
<form action="{{route('IRDOupdate')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="file" name="select_file">
    <input type="submit" value="IR更新系辦權限">
</form>
<form action="{{route('matchesImporter')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="file" name="select_file">
    <input type="submit" value="配對測試">
</form>

<form action="{{route('stepsImporter')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="file" name="select_file">
    <input type="submit" value="尺規匯入測試">
</form>

<form action="{{route('importGrades')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="text" name="department">
    <input type="text" name="BitemNum">
    <input type="file" name="select_file">
    <input type="submit" value="IR成績匯入測試">
</form>

<button onclick="location.href='{{route('export')}}'">匯出測試</button>
<button onclick="location.href='{{route('exportTNewdata')}}'">匯出老師</button>
<button onclick="location.href='{{route('exportHNewdata')}}'">匯出主任</button>
<button onclick="location.href='{{route('exportONewdata')}}'">匯出系辦</button>
<button onclick="location.href='{{route('exportSNewdata')}}'">匯出學生</button>
<button onclick="location.href='{{route('timesfetch')}}'">測試</button>

<button onclick="location.href='{{route('compareshow')}}'">compareshow</button>
<button onclick="location.href='{{route('showalldepartment')}}'">showalldepartment</button>
