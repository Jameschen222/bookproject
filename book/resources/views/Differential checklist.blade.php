@extends('admin.allHeaders')
@section('content')


<div id="app" class="pt-5 ">
    <div class="container">
        <div class="row my-2">
            <a href="/storage/Excel%20example/差分檢和.xlsx"><button type="button" name="" id=""
                    class="btn btn-success">下載Excel</button></a>
        </div>
        <div class="row">
            <table class="table">
                <tbody>
                    <tr>
                        <td>級距別</td>
                        <td>分數區間</td>
                        <td>區間機率%</td>
                        <td>人數比例</td>
                        <td>人數比例取2位小數</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>75以下</td>
                        <td>1.6</td>
                        <td>0.016</td>
                        <td>0.02</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>76-77</td>
                        <td>3.4</td>
                        <td>0.034</td>
                        <td>0.03</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>78-79</td>
                        <td>7.4</td>
                        <td>0.074</td>
                        <td>0.07</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>80-81</td>
                        <td>12.8</td>
                        <td>0.128</td>
                        <td>0.13</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>82-83</td>
                        <td>17.6</td>
                        <td>0.176</td>
                        <td>0.18</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>84-85</td>
                        <td>19.2</td>
                        <td>0.192</td>
                        <td>0.19</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>86-87</td>
                        <td>16.6</td>
                        <td>0.166</td>
                        <td>0.17</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>88-89</td>
                        <td>11.4</td>
                        <td>0.114</td>
                        <td>0.11</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>90-91</td>
                        <td>6.2</td>
                        <td>0.062</td>
                        <td>0.06</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>92-93</td>
                        <td>2.6</td>
                        <td>0.026</td>
                        <td>0.03</td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>94以上</td>
                        <td>1.2</td>
                        <td>0.012</td>
                        <td>0.01</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>100.0</td>
                        <td>1.000</td>
                        <td>1.00</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row" style="height: 300px;">
            <ve-histogram :data="chartData" width="100%"></ve-histogram>
        </div>
    </div>

</div>
@endsection
@section('script')
@parent
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/echarts/dist/echarts.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/v-charts/lib/index.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/v-charts/lib/style.min.css">
<script>
    new Vue({
        el:"#app",
        data () {
            return {
                chartData: {
                columns: ['級距別', '人數比例'],
                rows: [
                    { '級距別':1, '人數比例':0.02},
                    { '級距別':2, '人數比例':0.03},
                    { '級距別':3, '人數比例':0.07},
                    { '級距別':4, '人數比例':0.13},
                    { '級距別':5, '人數比例':0.18},
                    { '級距別':6, '人數比例':0.19},
                    { '級距別':7, '人數比例':0.17},
                    { '級距別':8, '人數比例':0.11},
                    { '級距別':9, '人數比例':0.06},
                    { '級距別':10, '人數比例':0.03},
                    { '級距別':11, '人數比例':0.01},
                ]
                }
            }
        }
    })
</script>
@endsection