@extends('admin.allHeaders')
@section('content')
<div id="app" class="container" @click="click">
  <div class="row">
    <div class="col-12 my-3">
      <label for="exampleFormControlSelect1">請選取科系</label>
      <select class="form-control" v-model="Department" @change="change">
        <option value="">請選擇</option>
        <template v-for="item in departments">
          <option :value="item.department">@{{ item.department}}</option>
        </template>
      </select>
    </div>
    <table class="table col-12 text-center">
      <thead>
        <tr>
          <th>管道</th>
          <th>科系</th>
          <th colspan="2">操作</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="item in times">
          <td scope="row">@{{item.project}}</td>
          <td>@{{item.department}}</td>
          <td><a
              :href="`fetchdatafortimes?department=${Department}&times=${(item.project!='')? '@!'+item.project :'empityTimes'}`"><button
                type="button" name="" id="" class="btn btn-success">下載Excel</button></a></td>
          <td><button type="button" name="" id="" class="btn btn-danger"
              onclick="return confirm('確認刪除此筆資料?');">刪除</button>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
@endsection
@section('script')
@parent
<script>
  new Vue({
      el: "#app",
      data() {
        return {
          departments: [],
          times: [],
          Department: ""
        };
      },
      methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
        async change() {
          const res = await axios.get(
            "http://irmaterials.nuu.edu.tw/fetchdatafortimes?department=" +
              this.Department
          );
          this.times = res.data;
        }
      },
      async created() {
          const res = await axios.get(
            "http://irmaterials.nuu.edu.tw/fetchdatafortimes"
          );
          this.departments = res.data;
      }
    });
</script>
@endsection