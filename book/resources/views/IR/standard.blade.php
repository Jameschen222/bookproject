@extends('admin.allHeaders')
@section('content')
<div id="app" class="pt-5 " @click="click">
    <div class="modal fade" id="InputForExcel" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">匯入</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row my-2">
                            <form action="{{route('importGrades')}}" method="post" enctype="multipart/form-data"
                                class="d-flex flex-column">
                                @csrf
                                <div class="form-group ">
                                    <label for="department">部門</label>
                                    <input type="text" name="department" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="BitemNum">Bitem數量</label>
                                    <input type="text" name="BitemNum" class="form-control" required>
                                </div>
                                <div class="d-flex flex-row">
                                    <input type="file" id="ExcelAdd" name="select_file" style="display: none"
                                        accept=".xlsx, .xls, .csv" v-on:change="onFileChange($event)" />
                                    <button type="button" class="btn btn-success" onclick="$('#ExcelAdd').click()">
                                        新增
                                    </button>
                                    <span
                                        class="mx-2 my-auto">新增資料在原本資料後方，不會將原資料刪除<br><small>@{{addname}}</small></span>

                                    <button type="submit" class="btn btn-success" v-if="addname!=''">送出</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        關閉
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row d-flex justify-content-between">

            <button type="button" name="" class="btn btn-primary mx-2" data-toggle="modal" data-target="#InputForExcel">
                匯入成績總表
            </button>
            <a href="/excel/exportForStandardDeviation">
                <button type="button" name="" class="btn btn-primary mx-2">
                    下載檔案
                </button>
            </a>

        </div>
        <div class="row my-2" v-if="!loading" >
            <div class="col-10">
                <label for="exampleFormControlSelect1">請選取科系</label>
                <select class="form-control" v-model="Department">
                    <option value="">請選擇</option>
                    <template v-for="item in departments">
                    <option :value="item.department">@{{ item.department}}</option>
                    </template>
                </select>
            </div>
            <div class="col-2 d-flex flex-column">
                <label for="exampleFormControlSelect1">查詢</label>
                <button type="button" class="btn btn-primary" @click="search">查詢</button>
            </div>
        </div>
        <div v-else class="row">
            <div class="col-12 pt-5">
                <div class="bouncing-loader ">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
        <div class="row my-2" v-if="!loading2">
            <div class="col-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="1" rowspan="1">老師帳號</th>
                            <th colspan="1" rowspan="1">標準差</th>
                            <th colspan="1" rowspan="1">部門</th>
                            <th colspan="1" rowspan="1">書審組別</th>
                            <th colspan="1" rowspan="1">名額類別</th>
                            <th colspan="1" rowspan="1">應試號碼</th>
                            <th colspan="1" rowspan="1">姓名</th>
                            <th colspan="1" rowspan="1">性別</th>
                            <template v-for="index in data.length>0?data[0].judgescores[0].scores.length:1">
                                <th colspan="1" rowspan="1">尺規</th>
                                <th colspan="1" rowspan="1">分數</th>
                            </template>

                            <th colspan="1" rowspan="1">總分</th>
                            <th colspan="1" rowspan="1">排名</th>
                        </tr>
                    </thead>
                    <tbody v-if="data.length>0">
                        <template v-for="(item,index) in data">
                            <template v-for="judgescore in item.judgescores">
                                <tr>
                                    <td colspan="1" rowspan="1">
                                        @{{item.Taccount}}
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        @{{item.standarddeviation}}
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        @{{judgescore.department}}
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        @{{judgescore.reviewClass}}
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        @{{judgescore.quotaClass}}
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        @{{judgescore.Snum}}
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        @{{judgescore.name}}
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        @{{judgescore.sex}}
                                    </td>

                                    <template v-for="score in judgescore.scores">
                                        <td colspan="1" rowspan="1">@{{score.Bitem}}</td>
                                        <td colspan="1" rowspan="1">@{{score.score}}</td>
                                    </template>
                                    <td>@{{addsum(judgescore.scores)}}</td>
                                    <td colspan="1">
                                        @{{judgescore.Remark}}
                                    </td>
                                </tr>
                            </template>
                        </template>
                    </tbody>
                    <tbody v-else>
                        <td colspan="1" rowspan="1">
                            無資料
                        </td>
                    </tbody>
                </table>
            </div>
            
        </div>
        <div v-else class="row">
            <div class="col-12 pt-5">
                <div class="bouncing-loader ">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@parent
<script>
    new Vue({
        el: "#app",        
        data() {
            return {
                data:[],
                loading:false,
                addname:"",
                departments:[],
                Department:"",
                loading2:false
            };
        },
        methods: {
            async click(){
                const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
            },
            onFileChange(e) {
                if (e.target.files.length != 0) {
                    this.addname = e.target.files[0].name;
                }
            },
            addsum(val){
                temp=0
                val.forEach(el => {
                    temp+=parseInt(el.score)
                });
                return temp
            },
            async search(){
                this.loading2=true
                const res=await axios.get("http://irmaterials.nuu.edu.tw/excel/show?department="+this.Department);
                this.data=res.data
                this.loading2=false
            }
        },
        created() {
            try {
                this.loading=true
                axios.get("http://irmaterials.nuu.edu.tw/excel/show"
                ).then(res => {
                    this.departments=res.data
                    this.loading=false
                });
            } catch (error) {
                alert("請先匯入成績總表")
                this.loading=false
            }
            
        }
    });
</script>
@endsection