@extends('admin.allHeaders')
@section('content')

<!-- Modal -->
<div class="modal fade" id="TypingInput" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">新增管理者資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('ForIR.store')}}" method="post">
                    @csrf
                    <div class="container">
                        <div class="row">
                            <div class="form-group">
                                <label for="">帳號</label>
                                <input type="text" name="Faccount" class="form-control" placeholder=""
                                    aria-describedby="helpId" v-model="type.account" required />
                                <small id="helpId" class="text-muted">輸入學校帳號</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="CloseInput">
                            關閉
                        </button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="changeinput" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">修改管理者資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('ForIR.update',0)}}" method="post">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="id" v-model="change.id">
                    @csrf
                    <div class="container">
                        <div class="row">
                            <div class="form-group">
                                <label for="">帳號</label>
                                <input type="text" name="Faccount" class="form-control" placeholder=""
                                    aria-describedby="helpId" v-model="change.account" required />
                                <small id="helpId" class="text-muted">輸入學校帳號</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="CloseInput">
                            關閉
                        </button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container" v-if="loading" @click="click">
    <div class="row">
        <div class="col-12">
            <button type="button" name="" class="btn btn-success mx-2" data-target="#TypingInput" data-toggle="modal">
                新增管理者資料
            </button>

        </div>
        <table class="table mt-2">
            <thead>
                <tr>
                    <th>帳號</th>
                    <th>姓名</th>
                    <th>修改</th>
                    <th>刪除</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(item,index) in all">
                    <td>@{{ item.Faccount }}</td>
                    <td>@{{ item.Fname }}</td>
                    <td>
                        <button type="button" name="" class="btn btn-info" @click="changedata(index)">
                            修改
                        </button>
                    </td>
                    <td>
                        <form action="{{route('ForIR.destroy',0)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            @csrf
                            <input type="hidden" name="id" v-model="item.id">
                            <button onclick="return confirm('確認刪除此筆資料?');" type="submit" name="" class="btn btn-danger">
                                刪除
                            </button>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div v-else class="container" style="height:80vh">
    <div class="row">
        <div class="col-12 pt-5">
            <div class="bouncing-loader ">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
@parent
<script>
    new Vue({
        el: "#app",
        data() {
            return {
                loading: false,
                type: {
                    Fname: "",
                    Faccount: "",
                    department: "",
                    date: ""
                },
                change: {
                    id: 0,
                    Fname: "",
                    Faccount: "",
                    department: "",
                    date: ""
                },
                all: [],
                addname: "",
                upname: "",
                key:""
            };
        },
        methods: {
            async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
            changedata(index) {
                this.change.id = this.all[index].id;
                this.change.name = this.all[index].Fname;
                this.change.account = this.all[index].Faccount;
                this.change.department = this.all[index].department;
                this.change.date = this.all[index].date;
                $("#changeinput").modal("show");
            },
            CloseInput() {
                //console.log("close");
                this.type.name = "";
                this.type.account = "";
                this.type.department = "";
                this.type.date = "";
                $("#TypingInput").modal("hide");
            }
        },
        computed: {
            getupdate() {
                var f = 123;


                return f.length != 0 ? true : false;
            }
        },
        created() {
            axios.get("http://irmaterials.nuu.edu.tw/ForIR/show").then(res => { //book.test
                this.all = res.data;
                this.loading = true;
            });
        }
    });
</script>
@endsection