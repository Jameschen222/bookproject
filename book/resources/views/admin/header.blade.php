<!DOCTYPE html>
<html lang="en">

<head>
    <title>管理者 建立系辦人員</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="icon" href="https://www.nuu.edu.tw/var/file/0/1000/msys_1000_9991812_60080.jpg" type="image/x-icon" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet" />
    <style>
        * {
            font-family: "Noto Sans TC", sans-serif;
        }

        @keyframes bouncing-loader {
            to {
                opacity: 0.1;
                transform: translate3d(0, -1rem, 0);
            }
        }

        .bouncing-loader {
            display: flex;
            justify-content: center;
        }

        .bouncing-loader>div {
            width: 1rem;
            height: 1rem;
            margin: 3rem 0.2rem;
            background: #8385aa;
            border-radius: 50%;
            animation: bouncing-loader 0.6s infinite alternate;
        }

        .bouncing-loader>div:nth-child(2) {
            animation-delay: 0.2s;
        }

        .bouncing-loader>div:nth-child(3) {
            animation-delay: 0.4s;
        }
    </style>
</head>

<body>

    <nav class="navbar navbar-expand-sm navbar-dark bg-dark" id="headers">
        <a class="navbar-brand" href="/admin">Admin</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
            aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"
                aria-hidden="true"></i> </button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item" :class="{ 'active': location[0]}">
                    <a class="nav-link" href="/admin">切換身分</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[1]}">
                    <a class="nav-link" href="/ForIR">給予IR權限</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[2]}">
                    <a class="nav-link" href="/adminTable">顯示table</a>
                </li>
            </ul>
            <div class="dropdown ml-auto">
                <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ Session::get('name')}}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="/logout">登出</a>
                </div>
            </div>
        </div>

    </nav>

    <!-- Button trigger modal -->

    <!-- Modal -->

    <section class="pt-5" id="app">
        @yield('content')
    </section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    @section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script>
        new Vue({
            el: "#headers",
            data() {
                return {
                    location: [false, false,, false]
                };
            },
            created() {
                var web = location.href.split("/");
                //console.log(web[web.length - 1])
                switch (web[web.length - 1]) {
                    case 'admin':
                        this.location[0] = true
                        break;
                    case 'ForIR':
                        this.location[1] = true
                        break;
                    default:
                        this.location[2] = true
                        break;
                }
            }

        })
    </script>

    @show

</body>

</html>