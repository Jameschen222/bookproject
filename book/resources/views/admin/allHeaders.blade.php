<!DOCTYPE html>
<html lang="en">

<head>
    <title>系務辦公室</title>
    <!-- Required meta tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="icon" href="https://www.nuu.edu.tw/var/file/0/1000/msys_1000_9991812_60080.jpg" type="image/x-icon" />


    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet" />
    <style>
        * {
            font-family: "Noto Sans TC", sans-serif;
        }

        @keyframes bouncing-loader {
            to {
                opacity: 0.1;
                transform: translate3d(0, -1rem, 0);
            }
        }

        .bouncing-loader {
            display: flex;
            justify-content: center;
        }

        .bouncing-loader>div {
            width: 1rem;
            height: 1rem;
            margin: 3rem 0.2rem;
            background: #8385aa;
            border-radius: 50%;
            animation: bouncing-loader 0.6s infinite alternate;
        }

        .bouncing-loader>div:nth-child(2) {
            animation-delay: 0.2s;
        }

        .bouncing-loader>div:nth-child(3) {
            animation-delay: 0.4s;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark" id="headers">
        @if(session()->get('status')=="head")
        <a class="navbar-brand" href="{{route('judge.index')}}">主任查看與評分</a>
        @elseif(session()->get('status')=="office")
        <a class="navbar-brand" href="{{route('head.index')}}">系務辦公室</a>
        @elseif(session()->get('status')=="Admin")
        <a class="navbar-brand" href="/admin">Admin</a>
        @elseif(session()->get('status')=="IR")
        <a class="navbar-brand" href="/flight">管理者 建立系辦人員</a>
        @else
        <a class="navbar-brand" href="/judge">書審老師評分</a>
        @endif

        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
            aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"
                aria-hidden="true"></i> </button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                @if(session()->get('status')=="head")
                <li class="nav-item" :class="{ 'active': location[5]}">
                    <a class="nav-link" href="/detail">榜單試算</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[7]}">
                    <a class="nav-link" href="/Differential_checklist">差分檢和試算表</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[8]}">
                    <a class="nav-link" href="/DepartmemtCompare">差分檢核</a>
                </li>
                @elseif(session()->get('status')=="office")
                <li class="nav-item" :class="{ 'active': location[0]}">
                    <a class="nav-link" href="/head">建立主任資料</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[1]}">
                    <a class="nav-link" href="/student">查看學生資料</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[2]}">
                    <a class="nav-link" href="/teacher">評審委員資料</a>
                </li>

                <li class="nav-item" :class="{ 'active': location[3]}">
                    <a class="nav-link" href="/match">分配評分人員</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[4]}">
                    <a class="nav-link" href="/standard">評分項目與分數區間</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[5]}">
                    <a class="nav-link" href="/detail">查看評分細節</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[7]}">
                    <a class="nav-link" href="/Differential_checklist">差分檢和試算表</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[8]}">
                    <a class="nav-link" href="/DepartmemtCompare">差分檢核</a>
                </li>
                @elseif(session()->get('status')=="Admin")
                <li class="nav-item" :class="{ 'active': location[0]}">
                    <a class="nav-link" href="/admin">切換身分</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[1]}">
                    <a class="nav-link" href="/ForIR">給予IR權限</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[2]}">
                    <a class="nav-link" href="/adminTable">顯示table</a>
                </li>
                @elseif(session()->get('status')=="IR")
                <li class="nav-item" :class="{ 'active': location[1]}">
                    <a class="nav-link" href="/student">學生資料</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[0]}">
                    <a class="nav-link" href="/flight">給予系辦權限</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[2]}">
                    <a class="nav-link" href="/Allgrade">所有系所評分資料</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[3]}">
                    <a class="nav-link" href="/importjudgedata">匯入計算標準差</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[4]}">
                    <a class="nav-link" href="/IRcompares">查看差分</a>
                </li>
                @else
                <li class="nav-item" :class="{ 'active': location[0]}">
                    <a class="nav-link" href="/judge">書審評分</a>
                </li>
                <li class="nav-item" :class="{ 'active': location[7]}">
                    <a class="nav-link" href="/Differential_checklist">差分檢和試算表</a>
                </li>
                @endif

            </ul>
            <div class="dropdown ml-auto d-flex flex-row">
                <div class="text-white w-100 my-auto">
                    剩餘時間 @{{time }}
                </div>
                <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ Session::get('name')}}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="/logout">登出</a>
                </div>
            </div>
        </div>
    </nav>

    <!-- Button trigger modal -->

    <!-- Modal -->

    <section class="mt-3" id="app">
        @yield('content')
    </section>

    @section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/element-ui/2.13.0/index.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/element-ui/2.13.0/locale/zh-TW.min.js'></script>
    <script>
        ELEMENT.locale(ELEMENT.lang.zhTW);
    </script>
    <script>
        new Vue({
            el: "#headers",
            data() {
                return {
                    location: [false, false, false, false, false, false, false, false],
                    time:"",
                    Ortime:"",
                    msg:"{{ cache('datastatus') }}"
                };
            },
            methods: {
                async getTime(){
                    const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequest")
                    this.Ortime=res.data
                    this.time=this.parseData(res.data)
                },

                async checktime(){
                    if(this.Ortime==60*5 || this.Ortime==60*3 || this.Ortime==60*1){
                        this.$message({
                            message: "請注意時間，盡快保存後登出，時間到自動套跳轉登出請注意!",
                            type: "warning"
                        });
                    }
                    if(this.Ortime<=0){
                        window.location.href = "http://irmaterials.nuu.edu.tw/logout";
                    }                    
                },
                timer() {
                    this.time = setInterval(async () => {
                        await this.getTime()
                        await this.checktime()
                    }, 1000)
                },
                parseData(duration){
                    var seconds = Math.floor(duration % 60),
                    minutes = Math.floor((duration / ( 60)) % 60),
                    hours = Math.floor((duration / ( 60 * 60)) % 24);
                    hours = (hours < 10) ? "0" + hours : hours;
                    minutes=(minutes < 10) ? "0" + minutes : minutes; 
                    seconds=(seconds < 10) ? "0" + seconds : seconds;
                    return hours + ":" + minutes + ":" + seconds ;
                },
                setTime() {
                    this.getTime()
                    this.timer()
                },
                stopTime() {
                    if (this.time) {
                        clearInterval(this.time)
                    }
                }
            },
            async created() {
                await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
                await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
                await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
                await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
                await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
                await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
                await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
                await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")

                var web = location.href.split("/");
                switch (web[web.length - 1]) {
                    case 'flight':
                    case 'head':
                    case 'judge':
                    case 'admin':
                        this.location[0] = true
                        break;
                    case 'ForIR':
                    case 'student':
                        this.location[1] = true
                        break;
                    case 'adminTable':
                    case 'student':
                    case 'teacher':
                    case 'Allgrade':
                    case 'allDepartmentScore':
                        this.location[2] = true
                        break;
                    case 'match':
                    case 'importjudgedata':
                        this.location[3] = true
                        break;
                    case 'standard':
                    case 'IRcompares':
                        this.location[4] = true
                        break;
                    case 'detail':
                        this.location[5] = true
                        break;
                    case 'Differential_checklist':
                        this.location[7] = true
                        break;
                    case 'DepartmemtCompare':
                        this.location[8] = true
                        break;
                    default:
                        this.location[6] = true
                        break;
                }
                await this.setTime()
                if(this.msg!=""){
                    this.$message({message: this.msg,duration:3000})
                }
            }               
        })
    </script>
    @show
</body>

</html>