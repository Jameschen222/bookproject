@extends('admin.allHeaders')
@section('content')

<div id="app" class="pt-5 container" @click="click">
  <!-- Modal -->
  <div class="modal fade" id="showModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">匯入舊有資料</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{route('jsupdate')}}" method="post">
            @csrf
            <div class="form-group">
              <label for="">請輸入此次管道名稱</label>
              <input type="text" name="newproject" class="form-control" placeholder="109學測" aria-describedby="helpId"
                required />
            </div>
            <input type="hidden" name="project" v-model="oldproject">
            <button type="submit" class="btn btn-primary">匯入</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- 匯入Excel -->
  <div class="modal fade" id="showModel2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">匯入Excel舊有資料</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{route('stepsImporter')}}" method="post" enctype="multipart/form-data" class="d-flex">
            @csrf
            <input type="file" id="ExcelAdd" name="select_file" style="display: none" accept=".xlsx, .xls, .csv"
              v-on:change="onFileChange($event)" />
            <button type="button" class="btn btn-success" onclick="$('#ExcelAdd').click()">
              新增
            </button>
            <span class="mx-2 my-auto">將資料匯入<br><small>@{{addname}}</small></span>
            <button type="submit" class="btn btn-primary" v-if="addname!=''">送出</button>
          </form>
        </div>
      </div>
    </div>
  </div>


  {{-- container --}}
  <div class="container mb-4">
    <div class="row">
      <div class="col-12 d-flex justify-content-between">
        <button class="btn btn-success mx-2" data-toggle="modal" data-target="#showModel2">
          匯入評分項目與分數區間
        </button>
        <a href="excel/exportonlysteps">
          <button class="btn btn-primary">
            匯出評分項目與分數區間
          </button>
        </a>
      </div>
      <div class="col-12 mt-2">
        <div class="d-flex flex-row">
          <H4>匯入歷史評分項目與分數區間 </H4>
        </div>
        <small>第一次使用此系統會顯示無資料 需等待下次評分</small>
      </div>
      <div class="col-12 my-2 d-flex justify-content-between">
        <div v-if="oldprojects.length>0">
          <template v-for="project in oldprojects">
            <button class="btn btn-info mx-2" @click="changeproject(project.login)">
              @{{project.login}}
            </button>
          </template>
        </div>
        <div v-else>
          無歷史資料
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div v-if="loading">
    <el-form :model="ruleForm" :rules="rules" ref="ruleForm" label-width="100px" class="demo-ruleForm">
      <el-row>
        <el-col :span="11" style="padding:1rem">
          <el-form-item label="管道" prop="project">
            <el-input v-model="ruleForm.project" placeholder="109學測"></el-input>
          </el-form-item>
        </el-col>
        <el-col :span="11" style="padding:1rem">
          <el-form-item label="總共面向" prop="allItems">
            <el-input v-model.number="ruleForm.allItems" type="number" min="1" placeholder="3"></el-input>
          </el-form-item>
        </el-col>
        <el-col :span="2" style="padding:1rem" v-if="!isNew">
          <el-button type="primary" @click="getItem()">送出</el-button>
        </el-col>
        <el-col :span="24">
          <div>加權公式說明: 面向一佔50% 有兩個分支 兩分支的分數相加*50% 以此類推 <span>EX:同學1 面向1-1 50分 面向1-2 10分 所得到的加權分數為30分</span></div>
        </el-col>
      </el-row>
      <el-row v-for="(item, i) in ruleForm.items" :key="i">
        <h4>面向@{{ i + 1 }}</h4>
        <el-col :span="6" style="padding:1rem">
          <el-form-item :prop="'items['+i+'].ItemsTitle'"
            :rules="[{ required: true, message: '請輸入標題', trigger: 'blur' }]" label="請輸入標題">
            <el-input v-model="item.ItemsTitle" placeholder="學習表現" type="string"></el-input>
          </el-form-item>
        </el-col>
        <el-col :span="7" style="padding:1rem">
          <el-form-item :prop="'items['+i+'].percent'"
            :rules="[{ required: true, message: '請輸入百分比'},{ type: 'number', message: '必須是數字'}]" label="請輸入百分比"
            label-width="130px">
            <el-input v-model.number="item.percent" placeholder="20" type="number" min="0"></el-input>
          </el-form-item>
        </el-col>
        <el-col :span="8" style="padding:1rem">
          <el-form-item :prop="'items['+i+'].ItemsNumber'"
            :rules="[{ required: true, message: '請輸入分支'},{ type: 'number', message: '必須是數字'}]"
            :label="`請輸入面向${i+1}需要幾個分支`" label-width="200px">
            <el-input v-model.number="item.ItemsNumber" type="number" min="0" placeholder="3"></el-input>
          </el-form-item>
        </el-col>
        <el-col :span="3" style="padding:1rem">
          <el-button type="primary" @click="getBreach(i)">送出</el-button>
        </el-col>
        <el-col :span="24" v-for="(score, j) in item.item" :key="j">
          <el-row>
            <el-col :span="8" style="padding:1rem">
              <el-form-item :prop="'items['+i+'].item['+j+'].number'"
                :rules="[{ required: true, message: '請輸入需要項目'},{ type: 'number', message: '必須是數字'}]"
                :label="`請輸入面向${i+1}-${j+1}需要項目`" label-width="175px">
                <el-input v-model.number="score.number" type="number" min="0" placeholder="3"></el-input>
              </el-form-item>
            </el-col>
            <el-col :span="16" style="padding:1rem">
              <el-button type="primary" @click="getScore(i,j)">送出</el-button>
            </el-col>
            <el-col :span="24">
              <el-row>
                <el-col :span="8" v-for="(ScoreDetail, k) in score.ScoreRange" :key="k">
                  <el-form-item :prop="'items['+i+'].item['+j+'].ScoreRange['+k+'].content'"
                    :rules="[{ required: true, message: '請輸入項目說明', trigger: 'blur' }]"
                    :label="`${i+1}-${j+1}-${k+1}內容`">
                    <el-input type="textarea" :autosize="{ minRows: 8, maxRows: 8}" placeholder="請輸入項目說明" type="string"
                      v-model="ScoreDetail.content">
                    </el-input>
                  </el-form-item>

                  <el-form-item :prop="'items['+i+'].item['+j+'].ScoreRange['+k+'].max'"
                    :rules="[{ required: true, message: '請輸入最高分'},{ type: 'number', message: '必須是數字'}]"
                    :label="`${i+1}-${j+1}-${k+1}最高分`">
                    <el-input type="number" min="0" v-model.number="ScoreDetail.max"></el-input>
                  </el-form-item>
                  <el-form-item :prop="'items['+i+'].item['+j+'].ScoreRange['+k+'].min'"
                    :rules="[{ required: true, message: '請輸入最低分'},{ type: 'number', message: '必須是數字'}]"
                    :label="`${i+1}-${j+1}-${k+1}最低分`">
                    <el-input type="number" min="0" v-model.number="ScoreDetail.min"></el-input>
                  </el-form-item>
                </el-col>
              </el-row>
            </el-col>
          </el-row>
        </el-col>
        <el-col :span="24">
          <hr />
        </el-col>
      </el-row>
      <el-row>
        <el-col :span="24">
          <el-form-item>
            <el-button type="primary" @click="submitForm('ruleForm')">儲存</el-button>
            <el-button @click="resetForm('ruleForm')">重置</el-button>
          </el-form-item>
        </el-col>
      </el-row>
    </el-form>
    <form action="{{route('standard.destroy',0)}}" method="post" ref="form">
      <input type="hidden" name="_method" value="DELETE" />
      @csrf
      <div style="display: none">
        <input type="text" name="project" v-model="ruleForm.project">
        <template v-for="(item, index1) in ruleForm.items">
          <template v-for="(getItem, index2) in item.item">
            <input type="text" style="width:60px;" name="Bitem[]" v-model="index1" />
            <input type="text" style="width:60px;" name="Mitem[]" v-model="index2" />
            <input type="text" style="width:60px;" name="Sitem[]" :value="0" />
            <input type="text" style="width:60px;" name="title[]" v-model="item.ItemsTitle" />
            <input type="text" style="width:60px;" name="percent[]" v-model="item.percent" />
            <input type="text" style="width:60px;" name="HighScore[]" />
            <input type="text" style="width:60px;" name="lowScore[]" /><br />
            <template v-for="(detail_content, index5) in getItem.ScoreRange">
              <input type="text" style="width:60px;" name="percent[]" value="null" />
              <input type="text" style="width:60px;" name="Bitem[]" v-model="index1" />
              <input type="text" style="width:60px;" name="Mitem[]" v-model="index2" />
              <input type="text" style="width:60px;" name="Sitem[]" :value="getIndex2(index5)" />
              <input type="text" style="width:60px;" name="title[]" :value="addBr(detail_content.content)" />
              {{-- -detail_content.content --}}
              <input type="text" style="width:60px;" name="HighScore[]" v-model="detail_content.max" />
              <input type="text" style="width:60px;" name="lowScore[]" v-model="detail_content.min" /><br />
            </template>
          </template>
        </template>
        <div class="text-center">
          <button type="submit" id="formButton" class="btn btn-primary my-2">送出並保存</button>
        </div>
      </div>
    </form>

  </div>
  <div v-else class="container" style="height:80vh">
    <div class="row">
      <div class="col-12 pt-5">
        <div class="bouncing-loader ">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('script')
@parent
<script>
  new Vue({
    el: "#app",
    data() {
      return {
        isNew:true,
        oldprojects:[],
        addname:"",
        ruleForm: {
          project: "",
          allItems: 1,
          items: [

          ]
        },
        rules: {
          project: [{ required: true, message: "請輸入內容", trigger: "blur" }],
          allItems: [
            { required: true, message: "請輸入內容", trigger: "blur" },
            { type: "number", message: "必須是數字" }
          ]
        },
        oldproject:"",
        loading: true        
      };
    },
    computed: {
      getIndex2() {
        return function(index) {
          return index + 1;
        };
      },
      addBr(){
        return function(index) {
          //console.log(index);
          return index.replace(/\n/g,"<br>");
        };
      }
    },
    // watch: {
    //   "ruleForm.allItems": function() {
    //     this.getItem();
    //   }
    // },
    methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
      changeproject(val){
        this.oldproject=val
        $('#showModel').modal('show')
      },
      onFileChange(e) {
          if (e.target.files.length != 0) {
              //console.log(e.target.files[0].name);
              this.addname = e.target.files[0].name;
          }
      },
      submitForm(){
        this.$refs["ruleForm"].validate(valid => {
          if (valid) {
            this.$refs.form.submit()  
          } else {
            //console.log("error submit!!");
            return false;
          }
        });
      },
      resetForm() {
        this.$refs['ruleForm'].resetFields();
      },
      getItem() {
        if (this.ruleForm.items.length < this.ruleForm.allItems) {
          for (
            let i = this.ruleForm.items.length;
            i < this.ruleForm.allItems;
            i++
          ) {
            this.$set(this.ruleForm.items,this.ruleForm.items.length,{
              percent: 0,
              ItemsTitle: "標題",
              ItemsNumber: 0,
              item: []
            })
          }
        } else {
          this.ruleForm.items.splice(
            this.ruleForm.allItems,
            this.ruleForm.items.length - this.ruleForm.allItems
          );
        }
        //console.log(this.ruleForm);
        
      },
      getBreach(i) {
        if (
          this.ruleForm.items[i].item.length <
          this.ruleForm.items[i].ItemsNumber
        ) {
          for (
            let k = this.ruleForm.items[i].item.length;
            k < this.ruleForm.items[i].ItemsNumber;
            k++
          ) {
            this.ruleForm.items[i].item.push({
              number: "",
              ScoreRange: []
            });
          }
        } else {
          this.ruleForm.items[i].item.splice(
            this.ruleForm.items[i].ItemsNumber,
            this.ruleForm.items[i].item.length -
              this.ruleForm.items[i].ItemsNumber
          );
        }
      },
      getScore(i, j) {
        if (
          this.ruleForm.items[i].item[j].ScoreRange.length <
          this.ruleForm.items[i].item[j].number
        ) {
          for (
            let k = this.ruleForm.items[i].item[j].ScoreRange.length;
            k < this.ruleForm.items[i].item[j].number;
            k++
          ) {
            this.ruleForm.items[i].item[j].ScoreRange.push({
              content: "",
              max: "",
              min: ""
            });
          }
        } else {
          this.ruleForm.items[i].item[j].ScoreRange.splice(
            this.ruleForm.items[i].item[j].number,
            this.ruleForm.items[i].item[j].ScoreRange.length -
              this.ruleForm.items[i].item[j].number
          );
        }
      }
    },
    async created() {
      this.loading = false
      const res =await axios.get("http://irmaterials.nuu.edu.tw/standard/show")
      this.ruleForm=await res.data ;
      this.ruleForm.allItems==0 ? '': res.data.allItems;
      this.oldprojects=res.data.oldprojects
      if (!res.data.items) {
        this.ruleForm.items=[]
        //console.log(this.ruleForm.items);
        this.isNew=true
      } else {
        this.ruleForm.items =await res.data.items;
        //console.log(this.ruleForm.items);
        
        this.isNew=false
      }


      
      await this.ruleForm.items.forEach(el => {
        el.item.forEach(el2 => {
          el2.ScoreRange.forEach(el3 => {
            el3.content=el3.content.replace(/<br>/g,"\n");
          });
        });
      });
      this.loading = true
    }
  });
</script>
@endsection