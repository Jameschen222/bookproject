@extends('admin.allHeaders')
@section('content')
<div class="container" v-if="!loading">
    <div class="row">
        <div class="col-12 my-3">
            <div class="row">
                <div class="col-8">
                    <label for="exampleFormControlSelect1">輸入差分</label>
                    <input type="text" v-model="dif" class="form-control">
                </div>
                <div class="col-2 d-flex flex-column">
                    <label for="exampleFormControlSelect1">查詢</label>
                    <button type="button" class="btn btn-primary" @click="search">查詢</button>
                </div>
            </div>
        </div>
        <div class="col-12" v-if="!loading2">
            <table class="table" v-if="data.length">
                <thead>
                    <tr>
                        <th>學生學號</th>
                        <th>學生姓名</th>
                        <th v-for="i in  data[0].teacher.length">評分老師與分數</th>
                    </tr>
                </thead>
                <tbody v-if="data.length">
                    <tr v-for="item in data">
                        <td scope="row">@{{item.studentSnum}}</td>
                        <td scope="row">@{{item.studentname}}</td>
                        <td v-for="teachers in item.teacher">@{{teachers.teacher}} : @{{teachers.sum}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div v-else class="container" style="height:80vh">
            <div class="row">
                <div class="col-12 pt-5">
                    <div class="bouncing-loader ">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div v-else class="container" style="height:80vh">
    <div class="row">
        <div class="col-12 pt-5">
            <div class="bouncing-loader ">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@parent

<script>
    new Vue({
        el: "#app",
        data() {
            return {
                loading:false,
                loading2:false,
                data:[],
                departments:[],
                dif:0,
                Department:"",

            };
        },
        methods: {
            async search(){
                this.loading2=true
                const res=await axios.get("http://irmaterials.nuu.edu.tw/compare?"+"dif="+this.dif);
                this.data=res.data
                this.loading2=false
            }
        },
        created() {
            this.search();          
        }
    });
</script>
@endsection