@extends('admin.allHeaders')
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
  integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<style>
  table tbody {
    height: 50vh;
    display: block;
    overflow-y: scroll;
  }

  table tbody::-webkit-scrollbar-track {
    padding: 2px 0;
    background-color: #FFF;
  }

  table tbody::-webkit-scrollbar {
    width: 10px;
  }

  table tbody::-webkit-scrollbar-thumb {
    border-radius: 10px;
    box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
    background-color: #737272;
  }

  table thead,
  tbody tr {
    display: table;
    width: 100%;
    table-layout: fixed;
  }

  td {
    word-break: break-all;
  }

  .selected {
    background-color: #ff9822;
    font-weight: bold;
  }

  table,
  th,
  td {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 6px;
    text-align: center;
  }

  table thead {
    width: calc(100% - 1em);
  }

  .name {
    width: 80px !important;
  }

  .thheader {
    width: 300px !important;
  }

  table th div {
    width: 20px;
    overflow: hidden;
    /* or auto */
  }

  .margin-rem {
    margin-right: 1rem;
  }

  .borderblod {
    border-right: 1px #000 solid !important;
  }

  .Graded {
    color: #aaaaaa;
  }

  .buttons i {
    position: fixed;
    font-size: 36px;
    padding: 0;
    margin: 0;
    cursor: pointer;
    background-color: rgba(255, 255, 255, 0.5);
  }

  .up {
    position: fixed;
    bottom: 45px;
    right: 45px;
  }

  .down {
    position: fixed;
    bottom: 0;
    right: 45px;
  }

  .left {
    position: fixed;
    bottom: 0px;
    right: 90px;
  }

  .right {
    position: fixed;
    bottom: 0;
    right: 0;
  }
</style>
<div id="score" @click="click">
  <div v-if="!loading">
    <div class="container mb-2" v-if="!await">
      <div class="row ">
        <div class="col-12 d-flex justify-content-end">
          <button class="btn btn-primary mx-2" @click="sorts(0)" v-if="clickone">以應試號碼排序(由大到小)</button>
          <button class="btn btn-primary mx-2" @click="sorts(0)" v-if="!clickone">以應試號碼排序(由小到大)</button>
          <button class="btn btn-primary mx-2" @click="sorts(1)">以總分排序</button>
          <button class="btn btn-primary mx-2" @click="sorts(2)">以加權總分排序</button>
          <button class="btn btn-success mx-2" @click="openDownload()">
            下載Excel
          </button>
        </div>
      </div>
      <div class="buttons">
        <i class="fas fa-angle-up up" @click="Scrolldown(-1)"></i>
        <i class="fas fa-angle-left left" @click="Scrolllleft(-1)"></i>
        <i class="fas fa-angle-down down" @click="Scrolldown(1)"></i>
        <i class="fas fa-angle-right right" @click="Scrolllleft(1)"></i>
      </div>
    </div>
    <div id="Mytable" class="table-responsive section-scroll" v-if="!await">
      <div>加權公式說明: 面向一佔50% 有兩個分支 兩分支的分數相加*50% 以此類推 <span>EX:同學1 面向1-1 50分 面向1-2 10分 所得到的加權分數為30分</span></div>
      <table class="text-center table table-bordered" v-if="all_item.length">
        <thead class="table-header">
          <tr>
            <th rowspan="2" colspan="11" class="borderblod" width="880px !important; ">
              基本資料
            </th>
            <template v-for="item in all_item">
              <th :colspan="row2(item)" :style="'width:'+ row2(item)*150 +'px !important;'" class="borderblod">
                @{{item.ItemsTitle}}(@{{item.percent}} %)
              </th>
            </template>
            <th rowspan="2" colspan="2" width="160px !important;">
              總分
            </th>
          </tr>
          <tr>
            <template v-for="(item, all_item_index) in all_item">
              <template v-for="itemIndex in item.item">
                <template v-for="(item,header_index) in itemIndex.ScoreRange">
                  <th colspan="2" @click="clickGetScore(item.max,item.min,item.max,item.min,getIndex(itemIndex))"
                    style="word-break: break-all;" class="thheader text-center"
                    :class="{borderblod:header_index==(itemIndex.ScoreRange.length-1)}" v-html="item.content">
                  </th>
                </template>
              </template>
            </template>
          </tr>
          <tr>
            <th class="name">應試號碼</th>
            <th class="name">名額類別</th>
            <th class="name">姓名</th>
            <th class="name">性別</th>
            <th class="name">考生身分</th>
            <th class="name">收入戶</th>
            <th class="name">通訊地址區域</th>
            <th class="name">畢業年度</th>
            <th class="name">畢業學校</th>
            <th class="name">學校編號</th>
            <th class="name borderblod">書審資料</th>
            <template v-for="(item, all_item_index) in all_item">
              <template v-for="item1 in item.item">
                <template v-for="(item,header_index) in item1.ScoreRange">
                  <th @click="clickGetScore(item.max,item.max,item.max,item.min,getIndex(item1))">@{{item.max}}</th>
                  <th @click="clickGetScore(item.min,item.min,item.max,item.min,getIndex(item1))"
                    :class="{borderblod:(header_index==(item1.ScoreRange.length-1))}">@{{item.min}}
                  </th>
                </template>
              </template>
            </template>
            <th class=" name">加總</th>
            <th class="name">加權後</th>
          </tr>
        </thead>
        <tbody id="Mytable2">
          <tr v-for=" (item,student_index) in student" @click="selectStudent(student_index)"
            :class="{selected:student_index==selectStudents,Graded:graded(student_index)&&student_index!=selectStudents  }">
            <td class="name">@{{item.Snum}}</td>
            <td class="name">@{{item.category}}</td>
            <td class="name">@{{item.name}}</td>
            <td class="name">@{{item.gender}}</td>
            <td class="name">@{{item.identifity}}</td>
            <td class="name">@{{item.income}}</td>
            <td class="name">@{{item.address.substr(0,6)}}</td>
            <td class="name">@{{item.year}}</td>
            <td class="name">@{{item.graduation}}</td>
            <td class="name ">@{{item.schoolnum}}</td>
            <td class="name borderblod"><a
                :href="`http://irmaterials.nuu.edu.tw/storage/student_data/${department}/${item.Snum}/${item.Snum}.pdf`"
                target="_blank">查看</a>
            </td>
            <template v-for="(item1, all_item_index1) in all_item">
              <template v-for="(item1, all_item_index2) in item1.item">
                <td :colspan="row(item1)" class="text-center  borderblod" style="width:auto">
                  <el-input-number v-model="item.score[getIndex(item1)]" :min="item.min[getIndex(item1)]"
                    :max="item.max[getIndex(item1)]" label="" v-if="canjudge">
                  </el-input-number>
                  <span v-else>@{{item.score[all_item_index1]}}</span>
                </td>
              </template>
            </template>
            <td class="name">@{{ sumScore(student_index) }}</td>
            <td class="name">@{{ sumScore2(student_index) }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="container my-2" v-if="!await">
      <div class="row">
        <div class="col-12 ">
          <button class="btn btn-block btn-primary py-2" v-if="canjudge" @click="onSubmit()">送出</button>
        </div>
      </div>
    </div>
  </div>
  <div v-else class="container" style="height:80vh">
    <div class="row">
      <div class="col-12 pt-5">
        <div class="bouncing-loader ">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  </div>
  <div v-if="await" class="container" style="height:80vh">
    <div class="row">
      <div class="col-12 d-flex flex-row ">
        <H1 class="text-center">請等待 系辦建置資料</H1>
      </div>
    </div>
  </div>
</div>
</div>


@endsection
@section('script')
@parent

<script>
  new Vue({
      el: "#score",
      data() {
        return {
          now: 0,
          all_item: [],
          student: [],
          item_length: [],
          loading: false,
          selectStudents: 0,
          department:"",
          await:false,
          clickone:false,
          canjudge:true,
          tempstudentscore:[]
        };
      },
      computed: {
        
        allitem() {
          return this.all_item.length;
        },
        sumScore() {
          return function(index) {
            var temp = 0;
            this.student[index].score.forEach(element => {
              temp += element;
            });
            return temp;
          };
        },
        
        sumScore2() {
          return function(studentindex) {
            var temp = 0
            var index = 0
            for (let i = 0; i < this.all_item.length; i++) {
              for (let j = 0; j < this.all_item[i].ItemsNumber; j++) {
                temp +=
                  (this.all_item[i].percent /this.all_item[i].ItemsNumber)* 0.01 *
                  this.student[studentindex].score[index]

                index++
              }
            }
            return temp.toFixed(2)
          }
        },
        graded(){
          return function(studentindex) {
            var temp = 0;
            for (let index = 0; index < this.student[studentindex].score.length; index++) {
              if(!this.student[studentindex].max[index]){
                return false
              }
            }
            return true;
          };
        }
      },
      methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
        openDownload(){
          window.open('/excel/export?teacher=1');
        },
        getIndex(item) {
            var count = 0
            var temp=0
            this.all_item.forEach(element => {
              element.item.forEach(el=>{
                if (item.number == el.number && el.ScoreRange==item.ScoreRange ) {
                  temp=count
                  return temp
                }
                count++;
              })
            })
            return temp
        },
        row(item) {
          var temp = 0
          item.ScoreRange.forEach(element => {
            temp += 1
          })
          // console.log(temp)
          return temp * 2
        },
        row2(item) {
          var temp = 0
          // console.log(item)

          item.item.forEach(el => {
            el.ScoreRange.forEach(element => {
              temp += 1
            })
          })
          return temp * 2
        },
        async onSubmit() {
          try {
            this.loading=true
            this.tempstudentscore=[]
            this.student.forEach(element => {
              for (let index = 0; index < element.id.length; index++) {
                var temp={}
                temp.id=element.id[index]
                temp.max=element.max[index]
                temp.min=element.min[index]
                temp.score=element.score[index]
                this.tempstudentscore.push(temp)
              }
            });
            const config = {
              url: '/judge/0',  // 只有此為必需
              method: 'put', // 大小寫皆可
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              baseURL: 'http://irmaterials.nuu.edu.tw/',
              data: this.tempstudentscore,
            }
            const res = await axios.request(config);
            this.loading = false;
            if(res.data=="Ok"){
              this.$message({message: "保存成功",type: 'success',duration:3000})
            }else{
              this.$message({message: "保存失敗 請確認資料",type: 'error',duration:3000})
              this.fetch();
            }
          } catch (error) {
            this.$message({message: "請確認身分",type: 'error',duration:3000})
            
            window.location.replace("http://irmaterials.nuu.edu.tw/logout");
          }
          
          
        },
        Scrolldown(val){
          var el=document.getElementById('Mytable2');
          el.scrollTop+=(val*50)
        },
        Scrolllleft(val){
          var el=document.getElementById('Mytable');
          el.scrollLeft+=(val*50)
        },
        changePage(tab) {
          this.now = tab.index;
        },
        clickGetScore(score1,score2,max, min, i) {
          //console.log(score1,score2,max, min, i);
          
          var score = score1 + score2;
          this.$set(this.student[this.selectStudents].max, i, max);
          this.$set(this.student[this.selectStudents].min, i, min);
          this.$set(
            this.student[this.selectStudents].score,
            i,
            parseInt(score / 2)
          );
        },
        selectStudent(student_index) {
          this.selectStudents = student_index;
        },
        async fetch(){
          this.loading = true;
          try {
            const res = await axios.get("http://irmaterials.nuu.edu.tw/judge/show");
            //const res = await axios.get("http://127.0.0.1:8000/judge/show");
            this.student = await res.data.student;
            this.department=await res.data.department;
            this.all_item = await res.data.all_item;
            this.await=!(res.data.issetstandard && res.data.issetmatch)

            if(this.await==true)reutrn;
            // this.all_item.forEach(element => {
            //   this.item_length.push(element.header.length * 2);
            // });
            // console.log(this.student.length);
            
            this.student.forEach(element => {
              console.log(element.min.length);
            });
            //console.log(this.all_item);
            this.canjudge=res.data.canjudge
            this.loading = false;
          } catch (error) {
            this.loading = false;
            this.await=true;
          }
        },
        async sorts(val) {
          if(val==0){
            this.clickone=!this.clickone
            for (let i = 0; i < this.student.length; i++) {
              this.student[i].All_total = parseInt(this.student[i].Snum);
            }
          }else{
            for (let i = 0; i < this.student.length; i++) {
              var temp = 0;
              for (let index = 0; index < this.student[i].score.length; index++) {
                temp+=(val==1?1:(this.all_item[index].percent*0.01)) *this.student[i].score[index]
              }
              this.student[i].All_total = temp;
            }
          }
          if(val==0 && this.clickone==true){
            this.student = await this.student.sort(function(a, b) {
              return a.All_total  > b.All_total ? 1 : -1;
            });
          }else{
            this.student = await this.student.sort(function(a, b) {
              return a.All_total  < b.All_total ? 1 : -1;
            });
          }
          this.$message({
            message: "排序完成",
            type: "success"
          });
        }
      },
      async created() {
        this.fetch()
      }
    });
</script>
@endsection