<!DOCTYPE html>
<html lang="en">

<head>
  <title>選擇身分</title>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet" />
  <style>
    * {
      font-family: "Noto Sans TC", sans-serif;
    }
  </style>
</head>

<body>
  <nav class="navbar navbar-expand-md navbar-dark bg-dark" id="headers" >
    <a class="navbar-brand" href="/judge">選擇身分</a>

    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
      aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fa fa-bars" aria-hidden="true"></i>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavId">
      <div class="dropdown ml-auto">
        <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          {{ Session::get('name')}}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="/logout">登出</a>
        </div>
      </div>
    </div>
  </nav>
  <div id="app" class=" mt-3">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <h1>選擇身分</h1>
        </div>
        <div class="col-12 ">
          <div class="row">
            <div class="col-4 mt-4 d-flex justify-content-center" v-for="item in data">
              <div class="card p-2" style="width: 18rem;">
                <img src="/storage/588023633.png" class="card-img-top" alt="..." />
                <div class="card-body">
                  <div>帳號:@{{item.Faccount}}</div>
                  <div>姓名:@{{item.Fname}}</div>
                  <div>系組:@{{item.department}}</div>
                  <div>身份:@{{item.login}}</div>
                </div>
                <form action="{{ route('SelectIdentify.update', 0) }}" method="post"
                  class="d-flex flew-row justify-content-center">
                  @csrf
                  <input type="hidden" name="_method" value="PUT" />
                  <input type="hidden" name="id" v-model="item.id" />
                  <button type="submit" class="btn btn-primary">選擇</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/element-ui/2.13.0/index.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
  <script>
    new Vue({
        el: "#app",
        data() {
          return {
            data: [
              
            ]
          };
        },
        methods: {
        async click(){
          const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },},
        created() {
          axios
            .get("http://irmaterials.nuu.edu.tw/SelectIdentify/show")
            .then(res => {
              ////console.log(res.data);
              this.data = res.data;
            });
        }
      });
  </script>
</body>

</html>