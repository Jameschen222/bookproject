<!DOCTYPE html>
<html lang="en">

<head>
    <title>管理者 建立系辦人員</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="icon" href="https://www.nuu.edu.tw/var/file/0/1000/msys_1000_9991812_60080.jpg" type="image/x-icon" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet" />
    <style>
        * {
            font-family: "Noto Sans TC", sans-serif;
        }

        @keyframes bouncing-loader {
            to {
                opacity: 0.1;
                transform: translate3d(0, -1rem, 0);
            }
        }

        .bouncing-loader {
            display: flex;
            justify-content: center;
        }

        .bouncing-loader>div {
            width: 1rem;
            height: 1rem;
            margin: 3rem 0.2rem;
            background: #8385aa;
            border-radius: 50%;
            animation: bouncing-loader 0.6s infinite alternate;
        }

        .bouncing-loader>div:nth-child(2) {
            animation-delay: 0.2s;
        }

        .bouncing-loader>div:nth-child(3) {
            animation-delay: 0.4s;
        }
    </style>
</head>

<body>

    <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
        <a class="navbar-brand text-white" >書審輔助系統</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars" aria-hidden="true"></i> </button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <div class="dropdown ml-auto">                
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="/logout">登出</a>
                </div>
            </div>
        </div>

    </nav>

    <!-- Button trigger modal -->

    <!-- Modal -->

    <section class="pt-5" id="app">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="jumbotron">
                    <h1 class="display-4">對不起</h1>
                    <p class="lead">您無此系統權限，請向相關單位詢問</p>
                    <hr class="my-4">
                    <a class="btn btn-primary btn-lg" href="/logout"" role="button">登出</a>
                </div>
                </div>
            </div>
        </div>        
    </section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

</body>

</html>
