<?php

namespace App\Http\Controllers;

use App\Exports\OrderExport;
use DummyFullModelClass;
use App\excel_export;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class Excellexport extends Controller
{
    public function export()
    {
        $cellData = [
            ['學號','姓名','成績'],
            ['10001','AAAAA','99'],
            ['10002','BBBBB','92'],
            ['10003','CCCCC','95'],
            ['10004','DDDDD','89'],
            ['10005','EEEEE','96'],
        ];
        return Excel::download(new OrderExport(),'test.xlsx');
    }

}
