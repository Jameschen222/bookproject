<?php

namespace App\Http\Controllers;

use App\Imports\UsersImport;
use DummyFullModelClass;
use App\Excelcontroll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Excelcontroll $excelcontroll
     * @return \Illuminate\Http\Response
     */
    public function index(Excelcontroll $excelcontroll)
    {
        //
    }

    public function importflightsDepartOffice(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx.csv'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;
        if ($data->count() > 0){
            foreach ($data as $ee) {
                foreach ($ee as $row) {
                    if ($check ==1){
                        $check=0;
                        continue;
                    }
                $insert_data[] = array(
                    'account' => $row[0],
                    'department' => $row[1],
                    'login' => "offic",
                    'date' => $row[2],
                );
            }
            }
        }

        if(!empty($insert_data)){
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是insert 系辦權限 表單
    public function importflightsDepartHead(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'account' => $row[0],
                        'department' => $row[1],
                        'login' => "dirhead",
                        'date' => $row[2],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是 insert 主任權限
    public function updateflightsDepartOffice(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'account' => $row[0],
                        'department' => $row[1],
                        'login' => "offic",
                        'date' => $row[2],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            foreach ($insert_data as $data) {
                DB::table('flights')->where('department',$data['department'])->delete();
            }
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是 update 系辦權限
    public function updateflightsDepartHead(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'account' => $row[0],
                        'department' => $row[1],
                        'login' => "dirhead",
                        'date' => $row[2],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            foreach ($insert_data as $data) {
                DB::table('flights')->where('department', $data['department'])->delete();
            }
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是 update 主任權限
    public function importstudents(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;
        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Snum' => $row[0],
                        'name' => $row[1],
                        'department' => $row[2],
                        'part' => $row[3],
                        'date' => $row[4],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            DB::table('students')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是匯入學生資料
    public function updatestudents(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Snum' => $row[0],
                        'name' => $row[1],
                        'department' => $row[2],
                        'part' => $row[3],
                        'date' => $row[4],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            foreach ($insert_data as $data) {
                DB::table('students')->where('department', $data['department'])->delete();
            }
            DB::table('students')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是更新學生資料
    public function importflightsDepartTeacher(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'account' => $row[0],
                        'department' => $row[1],
                        'login' => "dratea",
                        'date' => $row[2],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是給予老師權限
    public function updateflightsDepartTeacher(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'account' => $row[0],
                        'department' => $row[1],
                        'login' => "dratea",
                        'date' => $row[2],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            foreach ($insert_data as $data) {
                DB::table('flights')->where('department', $data['department'])->delete();
            }
            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是更新老師之權限
    public function importteacher(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Tnum' => $row[0],
                        'name' => $row[1],
                        'department' => $row[2],
                        'Account' => $row[3],
                        'date' => $row[4],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            DB::table('teachers')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是匯入書審老師資料
    public function updateteachers(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Tnum' => $row[0],
                        'name' => $row[1],
                        'department' => $row[2],
                        'Account' => $row[3],
                        'date' => $row[4],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            foreach ($insert_data as $data) {
                DB::table('teachers')->where('department', $data['department'])->delete();
            }
            DB::table('teachers')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是更新老師資料
    public function importmatches(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Snum' => $row[0],
                        'Tnum' => $row[1],
                        'choose' => $row[2],
                        'date' => $row[3],
                    );
                }
            }
        }

        if(!empty($insert_data)){
            DB::table('matches')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是匯入評分老師與學生配對

}