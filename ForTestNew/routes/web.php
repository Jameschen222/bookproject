<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('excel',function (){
    return view('excel');
});

/*匯入系辦權限*/Route::post('Exceltest/importflightsDepartOffice','ExcelController@importflightsDepartOffice')->name('iptOfDepartment');

/*匯入主任權限*/Route::post('Exceltest/importflightsDepartHead','ExcelController@importflightsDepartHead')->name('iptOfDepartHead');

/*匯入學生資料*/Route::post('excel/importstudents','ExcelController@importstudents')->name('iptOfStudents');

/*匯入老師資料*/Route::post('excel/importteacher','ExcelController@importteacher')->name('iptOfTeacher');

/*匯入老師權限*/Route::post('excel/importflightsDepartTeacher','ExcelController@importflightsDepartTeacher')->name('iptOfTeacheraccount');

/*更新系辦權限*/Route::post('excel1/updateflightsDepartOffice','ExcelController@updateflightsDepartOffice')->name('uptOfDepartOffice');

/*更新老師權限*/Route::post('excel1/updateflightsDepartTeacher','ExcelController@updateflightsDepartTeacher')->name('uptOfTeacheraccount');

/*匯入配對資料*/Route::post('excel1/importmatches','ExcelController@importmatches')->name('iptOfMatches');

/*更新主任權限*/Route::post('excel1/updateflightsDepartHead','ExcelController@updateflightsDepartHead')->name('uptOfDepartHead');

/*更新學生資料*/Route::post('excel1/updatestudents','ExcelController@updatestudents')->name('uptOfStudents');

/*更新老師資料*/Route::post('excel1/updateteachers','ExcelController@updateteachers')->name('uptOfTeachers');

Route::get('excel/export','Excellexport@export')->name('export');